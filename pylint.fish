#!/usr/bin/env fish

set PROJECT 'sepiida'
mkdir -p dist
set SOURCEFILES (find $PROJECT -maxdepth 3 -name "*.py" -not -path "*alembic/*" -print -not -path "*tests/*")
#set TESTFILES (find tests -maxdepth 3 -name "*.py" -print)
set TESTFILES (find ignorethis -maxdepth 3 -name "*.py" -print)
set FILES setup.py $SOURCEFILES $TESTFILES

for arg in $argv
    if [ $arg = '--recent' ]
        # get files from last 10 commits
        set GIT_DIFF (git diff --name-only HEAD~10 HEAD)
        # get unstaged files
        set GIT_DIFF $GIT_DIFF (git diff --name-only)
        # get staged files
        set GIT_DIFF $GIT_DIFF (git diff --staged --name-only)
        # get untracked files
        set GIT_DIFF $GIT_DIFF (git ls-files --others --exclude-standard)

        for FILE in $FILES
            if contains $FILE $GIT_DIFF
                set CHANGED_FILES $CHANGED_FILES $FILE
            end
        end
        set FILES $CHANGED_FILES
    end
end

echo $FILES
if not pylint $FILES --reports=no | tee dist/pylint.log
    echo "Linting failed"
    exit 1
end
