import hashlib
import json as JSON
import logging
from datetime import datetime, timedelta

import jwt as JWT
import pytest
from flask import Flask
from flask.ext.uuid import FlaskUUID  # pylint: disable=import-error, no-name-in-module

import sepiida.permissions
from sepiida.fixtures import (external_cert, external_key, fakeenv, httpretty, # pylint: disable=unused-import
                              internal_cert, internal_key, json_client, user)


def pytest_cmdline_main():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)

@pytest.fixture()
def app(settings):  # pylint: disable=redefined-outer-name
    my_app = Flask('sepiida')
    FlaskUUID(my_app)
    my_app.config['API_TOKEN']       = settings.API_TOKEN
    my_app.config['DEBUG']           = True
    my_app.config['SECRET_KEY']      = settings.SECRET_KEY
    my_app.config['SERVER_NAME']     = settings.SERVER_NAME
    my_app.config['TESTING']         = True
    my_app.config['TRUSTED_DOMAINS'] = settings.TRUSTED_DOMAINS
    sepiida.permissions.configure('sepiida.tests', 'https://users.service')
    return my_app

class JwtHelper():
    def __init__(self, audience, issuer, cert, key):
        self.audience = audience
        self.issuer = issuer
        self.cert = cert
        self.key = key

    def encode(self, payload, subject=None):
        utcnow = datetime.utcnow() - timedelta(seconds=300)
        utcnow = datetime.fromtimestamp(int(utcnow.timestamp()))

        body = {
            "aud": self.audience,
            "exp": utcnow + timedelta(seconds=900),
            "iat": utcnow,
            "iss": self.issuer,
            "nbf": utcnow,
        }

        if subject:
            body["sub"] = subject

        if payload:
            body["bdy"] = hashlib.sha256(JSON.dumps(payload).encode()).hexdigest()

        return {
            "payload": payload,
            "body": body,
            "jwt": self.basic_encode(body),
        }

    def basic_encode(self, body):
        return JWT.encode(body.copy(), self.key, algorithm='RS512')

    def decode(self, token):
        body = JWT.decode(
            token,
            self.cert,
            audience=self.audience,
            issuer=self.issuer,
        )

        body.update({
            "exp": datetime.utcfromtimestamp(body["exp"]),
            "iat": datetime.utcfromtimestamp(body["iat"]),
            "nbf": datetime.utcfromtimestamp(body["nbf"]),
        })

        return body

@pytest.yield_fixture()
def internal_jwt_helper(internal_cert, internal_key, mocker, settings): # pylint: disable=redefined-outer-name
    audience = "http://gigan.service/"
    issuer = "http://{}/".format(settings.SERVER_NAME)

    with mocker.patch("sepiida.jwt.read_secret_key", return_value=internal_key):
        yield JwtHelper(audience, issuer, internal_cert, internal_key)

@pytest.yield_fixture()
def external_jwt_helper(external_cert, external_key, mocker, settings): # pylint: disable=redefined-outer-name
    audience = "http://{}/".format(settings.SERVER_NAME)
    issuer = "http://gigan.service/"

    with mocker.patch("sepiida.jwt.read_secret_key", return_value=external_key):
        yield JwtHelper(audience, issuer, external_cert, external_key)

@pytest.fixture()
def internal_jwt(internal_jwt_helper): # pylint: disable=redefined-outer-name
    payload = {
        "username": "kpowers",
    }
    return internal_jwt_helper.encode(payload)

@pytest.fixture()
def external_jwt(external_jwt_helper): # pylint: disable=redefined-outer-name
    payload = {
        "username": "kpowers",
    }
    return external_jwt_helper.encode(payload)
