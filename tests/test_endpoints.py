import json
import uuid as UUID

import flask
import pytest

import sepiida.endpoints
import sepiida.errors
import sepiida.responses
import sepiida.session
from sepiida import fields


def test_uuid_endpoint(app, client):

    @app.route("/test-endpoint/<uuid:x>/")
    def _test_resource(x): # pylint: disable=unused-variable
        assert isinstance(x, UUID.UUID)
        return "", 200

    _uuid = UUID.uuid4()
    url = "/test-endpoint/{}/".format(_uuid)
    response = client.get(url)
    assert response.status_code == 200

    url = "/test-endpoint/invalid/"
    response = client.get(url)
    assert response.status_code == 404

def _setup_test_endpoint(app, settings=None, public_methods=None, response=None, signature=None):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        PUBLIC_METHODS = public_methods or []
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = signature
        @staticmethod
        def list():
            return {'status': 'ok'} if response is None else response
        @staticmethod
        def get(_id): # pylint: disable=unused-argument
            return {'status': 'ok'} if response is None else response
    TestEndpoint.add_resource(app)
    if settings:
        sepiida.session.register_session_handlers(app)

def test_endpoint_private(app, json_client, settings):
    _setup_test_endpoint(app, settings)

    response = json_client.get('/test-endpoint/1/')
    assert response.status_code == 403

def test_endpoint_public_list(app, json_client, settings):
    _setup_test_endpoint(app, settings, public_methods=['list'])

    response = json_client.get("/test-endpoint/")
    assert response.status_code == 200
    assert response.json() == {'status': 'ok'}

    response = json_client.get("/test-endpoint/42/")
    assert response.status_code == 403

def test_endpoint_public_get(app, json_client, settings):
    _setup_test_endpoint(app, settings, public_methods=['get'])

    response = json_client.get("/test-endpoint/42/")
    assert response.status_code == 200
    assert response.json() == {'status': 'ok'}

    response = json_client.get("/test-endpoint/")
    assert response.status_code == 403

def test_endpoint_invalid_public_method(app, settings):
    with pytest.raises(Exception) as e:
        _setup_test_endpoint(app, settings, public_methods=['retrieve'])

    assert str(e.value) == ("Method retrieve is not supported as PUBLIC_METHOD. "
                            "Supported methods are ['delete', 'get', 'list', 'post', 'put'].")

def test_endpoint_basic(app, json_client, json_client_session, settings):
    _setup_test_endpoint(app, settings)

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 403

    response = json_client_session.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json() == {'status': 'ok'}

def test_endpoint_fields_invalid(app, json_client):
    _setup_test_endpoint(app, signature=fields.Object(s={
        'status'    : fields.String(),
    }))
    response = json_client.get('/test-endpoint/?fields=invalid')
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'invalid-fields-query',
        'title' : ("You specified a fields value of 'invalid'. Fields must "
            "be specified as a comma-separated list of fields enclosed in '[' and ']'")
    }]}
    assert expected == response.json()

def test_endpoint_fields_unrecognized(app, json_client):
    _setup_test_endpoint(app, signature=fields.Object(s={
        'status'    : fields.String(),
    }))
    response = json_client.get('/test-endpoint/?fields=[unrecognized]')
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'invalid-field-specified',
        'title' : ("You specified the resource return 'unrecognized' but it is not a valid field for this resource. "
                   "Valid fields include 'resources', 'resources.status'"),
    }]}
    assert expected == response.json()

    response = json_client.get('/test-endpoint/?fields=[unrecognized1,unrecognized2]')
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'invalid-field-specified',
        'title' : ("You specified the resource return 'unrecognized1' but it is not a valid field for this resource. "
                   "Valid fields include 'resources', 'resources.status'"),
    },{
        'code'  : 'invalid-field-specified',
        'title' : ("You specified the resource return 'unrecognized2' but it is not a valid field for this resource. "
                   "Valid fields include 'resources', 'resources.status'"),
    }]}
    assert expected == response.json()

filter_test_set = [
    ('fields=[resources.a]',                [{'a'  : [{'b': 'x', 'c': 1}]}, {'a': [{'b': 'y', 'c': 2}]}]),
    ('fields=[resources.a.b]',              [{'a'  : [{'b': 'x',       }]}, {'a': [{'b': 'y',       }]}]),
    ('fields=[resources.a.b,resources.d]',  [{'a'  : [{'b': 'x'}], 'd': 'xxx'}, {'a': [{'b': 'y'}], 'd': 'yyy'}]),
]

@pytest.mark.parametrize('filter_, expected', filter_test_set)
def test_endpoint_list_fields_subset(app, expected, filter_, json_client):
    "Ensure that we can ask for a LIST on an endpoint with a subset of fields and get only those fields"
    response = [{
        'a'     : [{'b': 'x', 'c': 1}],
        'd'     : 'xxx',
    },{
        'a'     : [{'b': 'y', 'c': 2}],
        'd'     : 'yyy',
    }]
    _setup_test_endpoint(app, response=response, signature=fields.Object(s={
        'a'     : fields.Array(s=fields.Object(s={
            'b' : fields.String(),
            'c' : fields.Integer(),
        })),
        'd'     : fields.String(),
    }))
    response = json_client.get('/test-endpoint/?{}'.format(filter_))
    assert response.status_code == 200
    fullexpected = {'resources': expected}
    assert response.json() == fullexpected



#======
#Example filter to test
"""
https://erp.dev-auth.com/print/?filter[line_item]=https%3A%2F%2Fdata.dev-auth.com%2Fline-item%2F15fda26e-c4f9-4436-be50-50022130bfeb%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2F1a7e7235-146c-41c5-8f26-8914e5ae5162%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2F4d200c38-fe47-46e8-9ee7-b2dbacb7a439%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2Ff53cb093-5c1c-4d0c-906b-b5fad577bd49%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2F343b5390-7c78-4cdd-b875-426978f097fa%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2F82f2780d-4487-4593-bb5d-25259d4b9e07%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2F2275ffde-1cf8-4b52-b54f-eeeed265afbb%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2Ffdc51010-e8fb-47a4-a69b-c6debea6f4c1%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2Fde7e75e4-4247-4f75-9fdf-fbb6d5f8ef1b%2F%2Chttps%3A%2F%2Fdata.dev-auth.com%2Fline-item%2F171b6697-5705-43a6-a4e5-b173f267176b%2F
"""
filter_test_set2 = [
    ('fields=[resources.a]',                [{'a'  : [{'b': 'x', 'c': 1}]}, {'a': [{'b': 'y', 'c': 2}]}]),
    ('fields=[resources.a.b]',              [{'a'  : [{'b': 'x',       }]}, {'a': [{'b': 'y',       }]}]),
    ('fields=[resources.a.b,resources.d]',  [{'a'  : [{'b': 'x'}], 'd': 'xxx'}, {'a': [{'b': 'y'}], 'd': 'yyy'}]),
]

@pytest.mark.parametrize('filter_, expected', filter_test_set2)
def test_endpoint_list_fields_subset2(app, expected, filter_, json_client):
    "Ensure that we can ask for a LIST on an endpoint with a subset of fields and get only those fields"
    response = [{
        'a'     : [{'b': 'x', 'c': 1}],
        'd'     : 'xxx',
    },{
        'a'     : [{'b': 'y', 'c': 2}],
        'd'     : 'yyy',}
    ]
    _setup_test_endpoint(app, response=response, signature=fields.Object(s={
        'a'     : fields.Array(s=fields.Object(s={
            'b' : fields.String(),
            'c' : fields.Integer(),
        })),
        'd'     : fields.String(),
    }))
    response = json_client.get('/test-endpoint/?{}'.format(filter_))
    assert response.status_code == 200
    fullexpected = {'resources': expected}
    assert response.json() == fullexpected



#=====

def test_endpoint_fields_sparse(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'content'   : fields.String(),
            'status'    : fields.String(),
            'index'     : fields.Integer(),
        })
        @staticmethod
        def get(_id): # pylint: disable=unused-argument
            return {
                'content'   : 'some-content',
                'status'    : 'ok',
                'index'     : 0,
            }
    TestEndpoint.add_resource(app)

    response = json_client.get('/test-endpoint/1/')
    assert response.status_code == 200
    assert response.json() == {'content': 'some-content', 'index': 0, 'status': 'ok'}

    response = json_client.get('/test-endpoint/1/?fields=[content,status]')
    assert response.status_code == 200
    assert response.json() == {'content': 'some-content', 'status': 'ok'}

    response = json_client.get('/test-endpoint/1/?fields=[content, status]')
    assert response.status_code == 200
    assert response.json() == {'content': 'some-content', 'status': 'ok'}

def test_endpoint_fields_no_calculate(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'content'   : fields.String(),
            'status'    : fields.String(),
            'index'     : fields.Integer(),
        })
        def get(self, _id): # pylint: disable=unused-argument
            def _my_long_calculation():
                raise Exception("Should not be called")
            return {
                'content'   : 'some-content',
                'index'     : _my_long_calculation() if 'index' in self.fields else None,
                'status'    : 'ok'
            }
    TestEndpoint.add_resource(app)

    response = json_client.get('/test-endpoint/1/?fields=[content,status]')
    assert response.status_code == 200
    assert response.json() == {'content': 'some-content', 'status': 'ok'}

@pytest.mark.parametrize('queryargs, code, title', [
    ['filter=foo', 'empty-filter-name',
        "You requested a filter with no name. Filter requests should be of the form 'filter[property]'"],
    ['filterBob=foo', 'invalid-filter-request',
        "You requested a filter 'filterBob'. Filter requests should be of the form 'filter[property]'"],
    ['filter[Bob=foo', 'invalid-filter-request',
        "You requested a filter 'filter[Bob'. Filter requests should be of the form 'filter[property]'"],
    ['filterBob]=foo', 'invalid-filter-request',
        "You requested a filter 'filterBob]'. Filter requests should be of the form 'filter[property]'"],
    ['filter[bob]=foo', 'invalid-filter-property',
        "Your requested filter property, 'bob', is not a valid property for this endpoint. Please choose one of 'content', 'status'"],
    ['filter[content]=foo&filter[content]=bar', 'filter-property-specified-more-than-once',
        "Your requested filter property, 'filter[content]', was specified more than once. We don't currently support that"],
])
def test_endpoint_filter_errors(app, json_client, queryargs, code, title):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'content'   : fields.String(),
            'status'    : fields.String(),
        })
        def list(self): # pylint: disable=no-self-use
            assert False, "Should not reach this code on input {}".format(queryargs)

    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/?' + queryargs)
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : code,
        'title' : title,
    }]}
    assert response.json() == expected

def _sameish(one, two):
    """
    The ordering of our list items that is generated from our queryargs is undefined because of the way that flask handles
    queryargs. Because of that we have to be pretty lenient in our comparison when checking that the filters we get
    matches our expectations
    """
    for k, v in one.items():
        for x in v:
            assert x in two[k]
        assert len(two[k]) == len(v)
    for k, v in two.items():
        for x in v:
            assert x in one[k]
        assert len(one[k]) == len(v)
    return True

def _create_filter_endpoint(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'x': fields.String(),
            'y': fields.String(),
        })
        def get(self, _id):
            assert _id
            result = {k: [{
                    'operation' : _filter.operation,
                    'values'    : _filter.values,
                } for _filter in v
            ] for k, v in self.filters.items()}
            return flask.make_response(json.dumps(result))
        def list(self):
            result = {k: [{
                    'operation' : _filter.operation,
                    'values'    : _filter.values,
                } for _filter in v
            ] for k, v in self.filters.items()}
            return flask.make_response(json.dumps(result))

    TestEndpoint.add_resource(app)


@pytest.mark.parametrize('queryargs, expected', [
    ('', {}),
    ('filter[x]=abc',
        {'x': [{'operation': '=', 'values': ['abc']}]}),
    ('filter[x]=abc,xyz',
        {'x': [{'operation': '=', 'values': ['abc', 'xyz']}]}),
    ('filter[x]=abc&filter[y]<=xyz',
        {'x': [{'operation': '=', 'values': ['abc']}], 'y': [{'operation': '<=', 'values': ['xyz']}]}),
    ('filter[x]<abc,123&filter[y]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc', '123']}], 'y': [{'operation': '>=', 'values': ['xyz']}]}),
    ('filter[x]<abc&filter[x]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc']}, {'operation': '>=', 'values': ['xyz']}]}),
])
def test_endpoint_filter_happy_path(app, expected, json_client, queryargs):
    _create_filter_endpoint(app)
    response = json_client.get('/test-endpoint/1/?' + queryargs)
    assert response.status_code == 200
    assert _sameish(response.json(), expected)

@pytest.mark.parametrize('queryargs, expected', [
    ('', {}),
    ('filter[x]=abc',
        {'x': [{'operation': '=', 'values': ['abc']}]}),
    ('filter[x]=abc,xyz',
        {'x': [{'operation': '=', 'values': ['abc', 'xyz']}]}),
    ('filter[x]=abc&filter[y]<=xyz',
        {'x': [{'operation': '=', 'values': ['abc']}], 'y': [{'operation': '<=', 'values': ['xyz']}]}),
    ('filter[x]<abc,123&filter[y]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc', '123']}], 'y': [{'operation': '>=', 'values': ['xyz']}]}),
    ('filter[x]<abc&filter[x]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc']}, {'operation': '>=', 'values': ['xyz']}]}),
])
def test_endpoint_filter_resource_happy_path(app, expected, json_client, queryargs):
    _create_filter_endpoint(app)
    response = json_client.get('/test-endpoint/?' + queryargs)
    assert response.status_code == 200
    assert _sameish(response.json(), expected)

@pytest.mark.parametrize('queryargs, expected', [
    ('', {}),
    ('filter[x]=abc',
        {'x': [{'operation': '=', 'values': ['abc']}]}),
    ('filter[x]=abc,xyz',
        {'x': [{'operation': '=', 'values': ['abc', 'xyz']}]}),
    ('filter[x]=abc&filter[y]<=xyz',
        {'x': [{'operation': '=', 'values': ['abc']}], 'y': [{'operation': '<=', 'values': ['xyz']}]}),
    ('filter[x]<abc,123&filter[y]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc', '123']}], 'y': [{'operation': '>=', 'values': ['xyz']}]}),
    ('filter[x]<abc&filter[x]>=xyz',
        {'x': [{'operation': '<', 'values': ['abc']}, {'operation': '>=', 'values': ['xyz']}]}),
])
def test_endpoint_filter_in_body(app, expected, json_client, queryargs):
    _create_filter_endpoint(app)
    response = json_client.get('/test-endpoint/', data=queryargs)
    assert response.status_code == 200
    assert _sameish(response.json(), expected)

def test_endpoint_filter_bad_body(app, json_client):
    _create_filter_endpoint(app)
    response = json_client.get('/test-endpoint/', data=bytes.fromhex('deadbeef'))
    assert response.status_code == 400
    assert response.json() == {'errors': [{
        'code'  : 'bad-querystring-body',
        'title' : "'utf-8' codec can't decode byte 0xbe in position 2: invalid start byte",
    }]}

def _create_sort_endpoint(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'x': fields.String(),
            'y': fields.String(),
        })
        def get(self, _id):
            assert _id
            result = {'values': self.sorts}
            return flask.make_response(json.dumps(result))
        def list(self):
            result = {'values': self.sorts}
            return flask.make_response(json.dumps(result))

    TestEndpoint.add_resource(app)

@pytest.mark.parametrize('queryargs, expected', [
    ('', []),
    ('sort=x', [{'name': 'x', 'ascending': True}]),
    ('sort=-x', [{'name': 'x', 'ascending': False}]),
    ('sort=x, -y', [{'name': 'x', 'ascending': True}, {'name': 'y', 'ascending': False}]),
    ('sort=-y,-x', [{'name': 'y', 'ascending': False}, {'name': 'x', 'ascending': False}]),
])
def test_endpoint_sort_happy_path(app, expected, json_client, queryargs):
    _create_sort_endpoint(app)
    response = json_client.get('/test-endpoint/1/?' + queryargs)
    assert response.status_code == 200
    assert response.json()['values'] == expected

@pytest.mark.parametrize('queryargs, expected', [
    ('', []),
    ('sort=x', [{'name': 'x', 'ascending': True}]),
    ('sort=-x', [{'name': 'x', 'ascending': False}]),
    ('sort=x, -y', [{'name': 'x', 'ascending': True}, {'name': 'y', 'ascending': False}]),
    ('sort=-y,-x', [{'name': 'y', 'ascending': False}, {'name': 'x', 'ascending': False}]),
])
def test_endpoint_sort_resource_happy_path(app, expected, json_client, queryargs):
    _create_sort_endpoint(app)
    response = json_client.get('/test-endpoint/?' + queryargs)
    assert response.status_code == 200
    assert response.json()['values'] == expected

@pytest.mark.parametrize('queryargs, expected', [
    ('', []),
    ('sort=x', [{'name': 'x', 'ascending': True}]),
    ('sort=-x', [{'name': 'x', 'ascending': False}]),
    ('sort=x, -y', [{'name': 'x', 'ascending': True}, {'name': 'y', 'ascending': False}]),
    ('sort=-y,-x', [{'name': 'y', 'ascending': False}, {'name': 'x', 'ascending': False}]),
])
def test_endpoint_sort_in_body(app, expected, json_client, queryargs):
    _create_sort_endpoint(app)
    response = json_client.get('/test-endpoint/', data=queryargs)
    assert response.status_code == 200
    assert response.json()['values'] == expected

def test_endpoint_sort_bad_body(app, json_client):
    _create_sort_endpoint(app)
    response = json_client.get('/test-endpoint/', data=bytes.fromhex('deadbeef'))
    assert response.status_code == 400
    assert response.json() == {'errors': [{
        'code'  : 'bad-querystring-body',
        'title' : "'utf-8' codec can't decode byte 0xbe in position 2: invalid start byte",
    }]}

def test_endpoint_methods_list(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={
            'content'   : fields.String(),
        })
        @staticmethod
        def list():
            return [{'content': 'a'}, {'content': 'b'}]
    TestEndpoint.add_resource(app)

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [{'content': 'a'}, {'content': 'b'}]

    response = json_client.get('/test-endpoint/123/')
    assert response.status_code == 404

def test_endpoint_methods_get(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'content'   : fields.String(),
        })
        @staticmethod
        def get(uuid=None, _id=None):
            assert uuid is None or isinstance(uuid, UUID.UUID)
            assert _id is None or isinstance(_id, int)
            return {'content': 'a'}
    TestEndpoint.add_resource(app)

    response = json_client.get('/test-endpoint/123/')
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json() == {'content': 'a'}

    response = json_client.get('/test-endpoint/{}/'.format(UUID.uuid4()))
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json() == {'content': 'a'}

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 404

def _setup_put_endpoint(app, returns=None, signature=None):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = signature
        @staticmethod
        def put(uuid=None, _id=None):
            assert uuid is None or isinstance(uuid, UUID.UUID)
            assert _id is None or isinstance(_id, int)
            return returns
    TestEndpoint.add_resource(app)

def test_endpoint_methods_put_empty(app, json_client):
    _setup_put_endpoint(app)
    response = json_client.put('/test-endpoint/', json={'status': 1})
    assert response.status_code == 404

    response = json_client.put('/test-endpoint/123/', json={'status': 1})
    assert response.status_code == 204
    assert response.headers['Content-Type'] == 'text/plain'

def test_endpoint_methods_put_content(app, json_client):
    _setup_put_endpoint(app, returns={})
    response = json_client.put('/test-endpoint/123/', json={'status': 1})
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json() == {}

def test_endpoint_methods_put_no_content_disallowed(app, json_client):
    signature = sepiida.fields.Object(s={})
    _setup_put_endpoint(app, signature=signature)
    response = json_client.put('/test-endpoint/123/', json={})
    assert response.status_code == 400
    assert response.json() == {'errors': [{
        'code'  : 'empty-payload',
        'title' : 'You supplied an empty payload for your PUT request. This will do nothing',
    }]}

def test_endpoint_methods_patch(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def patch(uuid=None, _id=None):
            assert uuid is None or isinstance(uuid, UUID.UUID)
            assert _id is None or isinstance(_id, int)
            return None
    TestEndpoint.add_resource(app)

    response = json_client.patch('/test-endpoint/')
    assert response.status_code == 404

    response = json_client.patch('/test-endpoint/123/')
    assert response.status_code == 204

def test_endpoint_methods_post(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def post():
            return {'content': 'c'}, 201, {'Location': 'somewhere'}
    TestEndpoint.add_resource(app)

    response = json_client.post('/test-endpoint/123/', json={})
    assert response.status_code == 404

    response = json_client.post('/test-endpoint/', json={})
    assert response.status_code == 201
    assert response.json() == {'content': 'c'}
    assert response.headers['Content-Type'] == 'application/json'
    assert response.headers['Location'] == 'http://sepiida.service/somewhere'

def test_endpoint_methods_delete_with_defaults(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def delete(uuid=None, _id=None):
            assert uuid is None or isinstance(uuid, UUID.UUID)
            assert _id is None or isinstance(_id, int)
            return None
    TestEndpoint.add_resource(app)
    response = json_client.delete('/test-endpoint/')
    assert response.status_code == 204

    response = json_client.delete('/test-endpoint/123/')
    assert response.status_code == 204

def test_endpoint_methods_delete_without_defaults(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def delete(uuid):
            return uuid
    TestEndpoint.add_resource(app)
    response = json_client.delete('/test-endpoint/')
    assert response.status_code == 405
    expected = {
        'errors'    : [{
            'code'  : 'invalid-method',
            'title' : ('You attempted to request a DELETE on /test-endpoint/. '
                       'You must call DELETE on a specific resource rather than '
                       'the collection. You can do this by requesting a URL of the '
                       'pattern /test-endpoint/<uuid>/'),
        }]
    }
    assert response.json() == expected

class FakeError(Exception):
    pass

@pytest.mark.parametrize('error, expected_status, expected_error', [
    (sepiida.errors.Specification(FakeError, 401, 'integrity-error', 'bad'), 401, {'code': 'integrity-error', 'title': 'bad'}),
    (sepiida.errors.Specification(FakeError, 401, 'integrity-error'), 401, {'code': 'integrity-error', 'title': 'Bad stuff'}),
    (sepiida.errors.Specification(FakeError, 401), 401, {'code': 'FakeError', 'title': 'Bad stuff'}),
    (sepiida.errors.Specification(FakeError,), 400, {'code': 'FakeError', 'title': 'Bad stuff'}),
])
def test_error_handling(app, error, expected_error, expected_status, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()
        ERRORS = [error]
        @staticmethod
        def list():
            raise FakeError("Bad stuff")

    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == expected_status
    assert response.json() == {'errors': [expected_error]}

def test_error_handling_inheritance(app, json_client):
    class TestEndpointParent(sepiida.endpoints.APIEndpoint):
        ERRORS = [sepiida.errors.Specification(Exception, 402, 'hit-exception-parent')]

    class TestEndpointChild(TestEndpointParent):
        ENDPOINT = '/test-endpoint-child/'
        ERRORS = [sepiida.errors.Specification(FakeError, 401, 'hit-exception-child')]

        @staticmethod
        def get(_id):
            if _id == 1:
                raise FakeError('Bad stuff')
            else:
                raise Exception("Bad payload")

    TestEndpointChild.add_resource(app)

    response = json_client.get('/test-endpoint-child/1/')
    assert response.status_code == 401
    assert response.json() == {'errors': [{'code': 'hit-exception-child', 'title': 'Bad stuff'}]}

    response = json_client.get('/test-endpoint-child/2/')
    assert response.status_code == 402
    assert response.json() == {'errors': [{'code': 'hit-exception-parent', 'title': 'Bad payload'}]}

def test_callback_uri(app, json_client):
    class TestCallback(sepiida.endpoints.APIEndpoint):
        SIGNATURE = sepiida.fields.Object(s={
            'foo'   : sepiida.fields.String(),
        })
        ENDPOINT = '/run/<uuid:_uuid>/callback/'
        @staticmethod
        def post(_uuid, payload): # pylint: disable=unused-argument
            return flask.make_response('this is some content yo for {}'.format(_uuid))

    TestCallback.add_resource(app)

    _uuid = UUID.uuid4()
    response = json_client.post('/run/{}/callback/'.format(_uuid), json={'foo': 'bar'})
    assert response.status_code == 200
    assert response.data.decode('utf-8') == 'this is some content yo for {}'.format(_uuid)

def test_error_handling_unrecognized_error(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()
        @staticmethod
        def list():
            raise Exception("oh noes")

    app.config['DEBUG'] = False
    app.config['TESTING'] = False
    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 500

def test_nested_resource(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @staticmethod
        def post(payload): # pylint: disable=unused-argument
            raise Exception("oh noes")

        @staticmethod
        def list():
            raise Exception("oh no")

        @staticmethod
        def get():
            raise Exception("oh")

    class TestEndpointNested(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/<int:nest_id>/nested/'
        SIGNATURE = sepiida.fields.Object()

        @staticmethod
        def post(nest_id, payload): # pylint: disable=unused-argument
            return

        @staticmethod
        def list(nest_id): # pylint: disable=unused-argument
            return []

        @staticmethod
        def get(nest_id, _id): # pylint: disable=unused-argument
            return {}

    TestEndpoint.add_resource(app)
    TestEndpointNested.add_resource(app)

    response = json_client.post('/test-endpoint/12/nested/', json={})
    assert response.status_code == 204
    response = json_client.get('/test-endpoint/12/nested/')
    assert response.status_code == 200
    assert response.json()['resources'] == []
    response = json_client.get('/test-endpoint/12/nested/34/')
    assert response.status_code == 200
    assert response.json() == {}

def test_endpoint_name(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @staticmethod
        def get(_id):
            raise Exception(str(_id))

    TestEndpoint.add_resource(app, endpoint='mything')
    url = flask.url_for('mything', _id=12)
    assert url == '/test-endpoint/12/'

    uuid = UUID.uuid4()
    url = flask.url_for('mything', uuid=uuid)
    assert url == '/test-endpoint/{}/'.format(uuid)

def test_endpoint_auth_hook_no_action(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @classmethod
        def authenticate(cls):
            return

        @staticmethod
        def list():
            return

    sepiida.session.register_session_handlers(app)
    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 403

@pytest.mark.parametrize('error, expected_status', [
    (sepiida.errors.ResourceNotFound, 404),
    (sepiida.errors.InvalidPayload, 400),
    (sepiida.errors.Unauthorized, 403),
    (sepiida.errors.AuthenticationException, 403),
])
def test_endpoint_auth_hook_failed(app, json_client_session, error, expected_status):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @classmethod
        def authenticate(cls):
            raise error()

        @staticmethod
        def list():
            return

    sepiida.session.register_session_handlers(app)
    TestEndpoint.add_resource(app)
    response = json_client_session.get('/test-endpoint/')
    assert response.status_code == expected_status

def test_endpoint_auth_hook_auth(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object()

        @classmethod
        def authenticate(cls):
            return {'user': 'kenny'}

        @staticmethod
        def list():
            return [sepiida.session.current_user()]

    sepiida.session.register_session_handlers(app)
    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [{'user': 'kenny'}]

@pytest.mark.parametrize('result', (
    lambda: None,
    lambda: 'foo',
    lambda: ['foo'],
    lambda: flask.make_response(''),
))
def test_endpoint_caching_headers_default(app, json_client, result):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'

        @staticmethod
        def list():
            return result()

    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.headers['Cache-Control'] == 'max-age=10'

@pytest.mark.parametrize('result', (
    lambda: None,
    lambda: 'foo',
    lambda: ['foo'],
    lambda: flask.make_response(''),
))
def test_endpoint_caching_headers_manual(app, json_client, result):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        CACHING = {'GET': 'flubber'}
        @staticmethod
        def list():
            return result()

    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.headers['Cache-Control'] == 'flubber'


def test_options_with_queryargs_no_signature(app, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            return None
    TestEndpoint.add_resource(app)
    response = json_client.options('/test-endpoint/?foo=bar')
    assert response.status_code == 204

@pytest.mark.parametrize('arg, expected', (
    ['page[limit]=10', 10],
    ['', 20],
    ['page[limit]', 'empty-limit'],
    ['page[limit]=a', 'non-integer-limit'],
    ['page[limit]=2.1', 'non-integer-limit'],
))
def test_endpoint_limit(app, arg, expected, json_client):
    result = {
        'limit'     : None,
    }
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'x': fields.String(),
            'y': fields.String(),
        })
        def list(self):
            result['limit'] = self.limit
            return None
    TestEndpoint.add_resource(app)
    url = '/test-endpoint/?{}'.format(arg)
    response = json_client.get(url)
    if isinstance(expected, str):
        assert response.status_code == 400
        assert response.json()['errors'][0]['code'] == expected
    else:
        assert result['limit'] == expected

@pytest.mark.parametrize('arg, expected', (
    ['page[offset]=10', 10],
    ['', 0],
    ['page[offset]', 'empty-offset'],
    ['page[offset]=a', 'non-integer-offset'],
    ['page[offset]=2.1', 'non-integer-offset'],
))
def test_endpoint_offset(app, arg, expected, json_client):
    result = {
        'offset'     : None,
    }
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'x': fields.String(),
            'y': fields.String(),
        })
        def list(self):
            result['offset'] = self.offset
            return None
    TestEndpoint.add_resource(app)
    url = '/test-endpoint/?{}'.format(arg)
    response = json_client.get(url)
    if isinstance(expected, str):
        assert response.status_code == 400
        assert response.json()['errors'][0]['code'] == expected
    else:
        assert result['offset'] == expected

@pytest.mark.parametrize('_fields, expected', (
    [('a'),                     {'a': True}],
    [('a', 'b'),                {'a': True, 'b': True}],
    [('a', 'b.c'),              {'a': True, 'b': {'c': True}}],
    [('a', 'b.c.d'),            {'a': True, 'b': {'c': {'d': True}}}],
    [('a', 'b.c', 'b.d'),       {'a': True, 'b': {'c': True, 'd': True}}],
    [('a', 'b.c', 'b.d', 'e.f.g', 'e.f.h'),
     {'a': True, 'b': {'c': True, 'd': True}, 'e': {'f': {'g': True, 'h': True}}}],
))
def test_fields_to_selectors(_fields, expected):
    results = sepiida.endpoints._fields_to_selectors(_fields) # pylint: disable=protected-access
    assert results == expected

def test_list_count(app, json_client):
    "Test that we can return the total count on a resource LIST"
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = fields.Object(s={
            'i'    : fields.Integer(),
        })
        def list(self):
            return sepiida.responses.ResourceList(
                resources   = [{'i': i} for i in range(self.offset, min(30, self.limit + self.offset))],
                total_count = 30,
            )
    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    data = response.json()
    assert data['links']['next']
    assert not data['links']['prev']
    assert data['meta']['count'] == 30
    assert len(data['resources']) == 20

    response = json_client.get(data['links']['next'])
    assert response.status_code == 200
    data = response.json()
    assert not data['links']['next']
    assert data['links']['prev']
    assert data['meta']['count'] == 30
    assert len(data['resources']) == 10
