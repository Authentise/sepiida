import uuid

import pytest

import sepiida.endpoints
import sepiida.routing


@pytest.mark.parametrize('url', [
    '/test-extract-parameters/1/sub-thing/foo/',
    'http://localhost/test-extract-parameters/1/sub-thing/foo/',
    'https://localhost/test-extract-parameters/1/sub-thing/foo/',
    'http://else.wh.er/test-extract-parameters/1/sub-thing/foo/',
])
def test_extract_parameters_happy_path(app, url):
    @app.route('/test-extract-parameters/<int:some_int>/sub-thing/<a_string>/')
    def _my_endpoint(some_int, a_string): # pylint: disable=unused-argument, unused-variable
        return '', 200
    name, parameters = sepiida.routing.extract_parameters(app, 'GET', url)
    assert name == '_my_endpoint'
    assert parameters == {'a_string': 'foo', 'some_int': 1}

def test_extract_paramters_unknown(app):
    name, properties = sepiida.routing.extract_parameters(app, 'GET', '/unknown-thing/')
    assert name is None
    assert properties == {}

def test_extract_paramters_with_redirect(app):
    # We need the root endpoint here because the path for an empty string will
    # match a root rule but the rule will require a slash at the end
    # so we get a permanent redirect to add the slash which is what we're testing
    @app.route('/')
    def _my_endpoint(): # pylint: disable=unused-variable
        return '', 200
    name, properties = sepiida.routing.extract_parameters(app, 'GET', '')
    assert name == '_my_endpoint'
    assert properties == {}

def test_extract_parameters_none(app):
    @app.route('/test-extract-parameters-none/')
    def _my_endpoint(): # pylint: disable=unused-argument, unused-variable
        return '', 200
    name, parameters = sepiida.routing.extract_parameters(app, 'GET', '/test-extract-parameters-none/')
    assert name == '_my_endpoint'
    assert parameters == {}

def test_extract_parameters_mismatched_server_name(app):
    app.config['SERVER_NAME'] = 'some.bad.place'
    @app.route('/test-extract-parameters-mismatch/')
    def _my_endpoint(): # pylint: disable=unused-argument, unused-variable
        return '', 200
    name, parameters = sepiida.routing.extract_parameters(app, 'GET', '/test-extract-parameters-mismatch/')
    assert name == '_my_endpoint'
    assert parameters == {}

def test_extract_uuid(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/some-uuid-resource/'

        @staticmethod
        def get(_uuid):
            return _uuid
    TestEndpoint.add_resource(app, 'some-uuid-resource')

    _uuid = uuid.uuid4()
    uri = sepiida.routing.uri('some-uuid-resource', _uuid)
    assert sepiida.routing.extract_uuid(uri) == _uuid

def test_extract_uuid_invalid_url(app):
	assert sepiida.routing.extract_uuid('https://sepiida.service/') is None
	assert sepiida.routing.extract_uuid('https://sepiida.service/invalid/') is None

def test_uri(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def get(_uuid):
            return _uuid
    TestEndpoint.add_resource(app, 'some-resource')
    _uuid = uuid.uuid4()
    uri = sepiida.routing.uri('some-resource', _uuid)
    assert uri == 'https://sepiida.service/test-endpoint/{}/'.format(_uuid)

def test_uri_list(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            return None
    TestEndpoint.add_resource(app, 'some-resource')
    _uuid = uuid.uuid4()
    with pytest.raises(sepiida.errors.RoutingError) as e:
        sepiida.routing.uri('some-resource', _uuid)
    assert str(e.value) == (
        "Failed to build a URI. You may have attempted to create a URI on a resource (some-resource) "
        "that does not have a get() method or supplied a UUID when it expects an integer ID or vice-versa. "
        "Root error: Could not build url for endpoint 'some-resource' ('GET') with values ['_single_resource', "
        "'uuid']."
    )

def test_uri_put(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def put(_uuid):
            return _uuid
    TestEndpoint.add_resource(app, 'some-resource')
    _uuid = uuid.uuid4()
    uri = sepiida.routing.uri('some-resource', _uuid, method='PUT')
    assert uri == 'https://sepiida.service/test-endpoint/{}/'.format(_uuid)
