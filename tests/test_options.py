import json

import flask
import pytest
import yaml

import sepiida.endpoints
import sepiida.fields
import sepiida.options


def _soft_update(destination, source):
    "Recursively apply update() from one dict to another, but don't annihilate sub-dicts"
    for k, v in source.items():
        if isinstance(v, dict):
            _soft_update(destination[k], v)
        else:
            destination[k] = v

def _get_raml(client, headers=None):
    response = client.options('/', headers=headers)
    assert response.status_code == 200
    assert response.content_type == 'application/raml+yaml'
    data = response.data.decode('utf-8')
    return yaml.load(data)

def _add_test_endpoint(app, signature=None):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        "Some test endpoint"
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = signature
        @staticmethod
        def list():
            "A list function"
            return flask.make_response('', 200)
    TestEndpoint.add_resource(app)

@pytest.mark.parametrize('headers', (
    None,
    {'Accept': 'text/raml'},
))
def test_options_base(app, client, headers):
    _add_test_endpoint(app)
    sepiida.options.enable(app, 'Test App', 'http://test.service', '0.0.0')

    data = _get_raml(client, headers)
    expected = {
        '/test-endpoint/': {
            'description'   : 'Some test endpoint',
            'get'           : {
                'body'      : {
                    'application/json'  : {}
                },
                'description'   : 'A list function',
                'responses'     : {
                    '200'       : {
                        'body'  : {
                            'application/json'  : {
                                'example'       : None,
                                'schema'        : None,
                            }
                        },
                        'description'   : 'The request was successful',
                    }
                }
            }
        },
        'baseUri'       : 'http://test.service',
        'documentation' : [{'content': '', 'title': ''}],
        'mediaType'     : 'application/json',
        'title'         : 'Test App',
        'version'       : '0.0.0',
    }
    assert data == expected

def test_bad_accept(app, client):
    _add_test_endpoint(app)
    sepiida.options.enable(app, 'Test App', 'http://test.service', '0.0.0')
    response = client.options('/', headers={'Accept': 'application/json'})
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'unable-to-serialize',
        'title' : ('No serializers are able to respond in a format you would accept. '
                   'Check your Accept header and alter it to accept more types for this request'),
    }]}
    assert response.json == expected

def test_options_not_enabled(app, client):
    _add_test_endpoint(app)
    response = client.options('/', headers={'Accept': 'application/json'})
    assert response.status_code == 404


@pytest.mark.parametrize('signature, expected', [
     (sepiida.fields.String(choices=['foo', 'bar']), 'foo|bar'),
     (sepiida.fields.String(choices=['foo', 'bar'], example='baz'), 'baz'),
     (sepiida.fields.String(example='bar'), 'bar'),
])
def test_example_generation(app, client, expected, signature):
    signature = sepiida.fields.Object(s={
        'foo'   : signature,
    })
    _add_test_endpoint(app, signature)
    sepiida.options.enable(app, 'Test App', 'http://test.service', '0.0.0')
    data = _get_raml(client)
    expected_example = {'foo': expected}

    assert json.loads(data['/test-endpoint/']['get']['body']['application/json']['example']) == expected_example
    assert json.loads(data['/test-endpoint/']['get']['responses']['200']['body']['application/json']['example']) == expected_example

def test_schema_generation_get(app, client):
    signature = sepiida.fields.Object(s={
        'foo'   : sepiida.fields.String(choices=['bar'], docs="A foo", example='bar')
    }, docs="The basic signature")
    _add_test_endpoint(app, signature)
    sepiida.options.enable(app, 'Test App', 'http://test.service', '0.0.0')
    data = _get_raml(client)

    expected_schema = {
        '$schema'       : 'http://json-schema.org/draft-03/schema',
        'description'   : "The basic signature",
        'example'       : '{"foo": "bar"}',
        'filterable'    : True,
        'nullable'      : False,
        'searchable'    : True,
        'properties'    : {
            'foo'       : {
                'description'   : 'A foo',
                'enum'          : ['bar'],
                'example'       : '"bar"',
                'filterable'    : True,
                'nullable'      : False,
                'searchable'    : True,
                'sortable'      : True,
                'type'          : 'string',
            },
        },
        'required'  : ['foo'],
        'sortable'  : True,
        'type'      : 'object',
    }

    schema = json.loads(data['/test-endpoint/']['get']['body']['application/json']['schema'])
    assert schema == expected_schema
    schema = json.loads(data['/test-endpoint/']['get']['responses']['200']['body']['application/json']['schema'])
    assert schema == expected_schema

class OddClass():
    def __str__(self):
        return 'blub'

@pytest.mark.parametrize('signature,updates', (
    [{'foo': sepiida.fields.String(docs="A foo", example='bar')},
        {'required': ['foo']}],
    [{'foo': sepiida.fields.String(docs="A foo", example='bar', default='bar')},
        {'properties': {'foo': {'default': 'bar'}}}],
    [{'foo': sepiida.fields.String(docs="A foo", example='bar', default='bar', choices=['bar', 'baz'])},
        {'properties': {'foo': {'default': 'bar', 'enum': ['bar', 'baz']}}}],
    [{'foo': sepiida.fields.String(docs="A foo", example='bar', default='bar', choices=[OddClass()])},
        {'properties': {'foo': {'default': 'bar', 'enum': ['blub']}}}],
))
def test_schema_generation_post(app, client, signature, updates):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        "Some test endpoint"
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s=signature, docs="The basic signature")

        @staticmethod
        def post():
            "A post function"
            return flask.make_response('', 200)

    TestEndpoint.add_resource(app)
    sepiida.options.enable(app, 'Test App', 'http://test.service', '0.0.0')
    data = _get_raml(client)

    expected_schema = {
        '$schema': 'http://json-schema.org/draft-03/schema',
        'description'   : "The basic signature",
        'example'       : '{"foo": "bar"}',
        'filterable'    : True,
        'nullable'      : False,
        'searchable'    : True,
        'properties'    : {
            'foo'       : {
                'description'   : 'A foo',
                'example'       : '"bar"',
                'filterable'    : True,
                'nullable'      : False,
                'searchable'    : True,
                'sortable'      : True,
                'type'          : 'string',
            },
        },
        'required'  : [],
        'sortable'  : True,
        'type'      : 'object',
    }
    # Depending on our parametrization we need to include some additional data in the properties
    _soft_update(expected_schema, updates)
    schema = json.loads(data['/test-endpoint/']['post']['body']['application/json']['schema'])
    assert schema == expected_schema

@pytest.mark.parametrize("error_spec, status_code, description", [
    ([sepiida.errors.Specification(Exception)], 400, "Exception raised"),
    ([sepiida.errors.Specification(TypeError, 401)], 401, "TypeError raised"),
    ([sepiida.errors.Specification(ValueError, 402, error_code='some-code')], 402, '`some-code`'),
    ([sepiida.errors.Specification(KeyError, 403, title='some title')], 403, 'some title'),
    ([sepiida.errors.Specification(Exception, 404, error_code='some-code', title='some title')], 404, '`some-code`: some title'),
    ([sepiida.errors.Specification(Exception, 405, error_code='some-code', title='some title', docs='the docs')], 405, 'the docs'),
    ([
        sepiida.errors.Specification(TypeError, 406, error_code='some-code', title='some title'),
        sepiida.errors.Specification(ValueError, 406, error_code='other-code', title='another title', docs='the docs'),
     ],  406, '`some-code`: some title\nthe docs'),
])
def test_response_description(app, client, description, error_spec, status_code):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        ERRORS = error_spec
        def post(self):
            pass

    TestEndpoint.add_resource(app)
    sepiida.options.enable(app, 'Test App', 'http://test.service', '0.0.0')
    data = _get_raml(client)

    expected_description = {'description': description}
    actual = data['/test-endpoint/']['post']['responses'][str(status_code)]
    assert actual == expected_description
