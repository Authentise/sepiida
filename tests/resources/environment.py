import sepiida.environment

SPECIFICATION = sepiida.environment.Specification(
	EXAMPLE_SECRET_VIA_DOCKER = sepiida.environment.Variable('default value without docker secret'),
)


def parse():
    return sepiida.environment.parse(SPECIFICATION)
