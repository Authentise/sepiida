import io
import itertools
import json
import os
from uuid import uuid4

import pytest
import requests
from flask import current_app, make_response, request, session

import sepiida.endpoints
import sepiida.permissions
import sepiida.storage


def test_json_client_post_data(json_client):
    with pytest.raises(Exception) as e:
        json_client.post('http://some.where', data='my data', json={})
    assert str(e.value) == "Don't specify a data parameter when using the JSONClient"

def test_json_client_post_happy_path(app, json_client):
    @app.route('/test-json-client-post-happy-path', methods=['POST'])
    def _handle_post(): # pylint: disable=unused-variable
        assert request.content_type == 'application/json'
        assert request.json == {'test': 'a'}
        return '', 201
    response = json_client.post('/test-json-client-post-happy-path', json={'test': 'a'})
    assert response.status_code == 201
    assert response.text == ''

def test_json_client_options_happy_path(app, json_client):
    expected_json = {'test': 'a'}
    @app.route('/test-json-client-options-happy-path', methods=['OPTIONS'])
    def _handle_options(): # pylint: disable=unused-variable
        assert request.content_type == 'application/json'
        return json.dumps(expected_json), 200
    response = json_client.options('/test-json-client-options-happy-path')
    assert response.ok
    assert response.json() == expected_json

def test_httpretty(httpretty):
    httpretty.register_uri(
            httpretty.POST,
            'http://somewhere.com/',
            location='foobar')
    response = requests.post('http://somewhere.com/', data='my data')
    assert response.headers['Location'] == 'foobar'

def test_permission_created(permission):
    kwargs = {
        'holder'        : 'fake-holder',
        'namespace'     : 'test',
        'object_'       : 'fake-resource',
        'resource_name' : 'a name',
        'right'         : 'some-right',
    }
    uri = sepiida.permissions.create(**kwargs)
    assert permission.created(**kwargs)
    assert permission.grants[uri].created_by == 'unknown'
    assert permission.counters.POST == 1

def _create_permission_endpoint(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def post():
            uri = sepiida.permissions.create(
                holder          = 'fake-holder',
                namespace       = 'test',
                object_         = 'fake-resource',
                resource_name   = 'some name',
                right           = 'some-right',
            )
            return make_response(uri)

    TestEndpoint.add_resource(app)

def test_permission_created_session(app, json_client_session, permission, user):
    "Ensure we can capture the created_by field when we have a proper session"
    _create_permission_endpoint(app)
    response = json_client_session.post('/test-endpoint/', json={})
    assert response.status_code == 200
    uri = response.data.decode()
    assert permission.grants[uri].created_by == user['uri']
    results = sepiida.permissions.search()
    assert results == [{
        'created_by'    : user['uri'],
        'holder'        : 'fake-holder',
        'namespace'     : 'test',
        'object'        : 'fake-resource',
        'resource_name' : 'some name',
        'right'         : 'some-right',
        'uri'           : uri,
    }]

@pytest.mark.usefixtures('app')
def test_permission_created_privileged(permission):
    kwargs = {
        'holder'        : 'fake-holder',
        'namespace'     : 'test',
        'object_'       : 'fake-resource',
        'resource_name' : 'some name',
        'right'         : 'some-right',
    }
    sepiida.permissions.create(
        privileged=True,
        **kwargs
    )
    assert permission.created(
        privileged=True,
        **kwargs
    )
    assert not permission.created(
        privileged=False,
        **kwargs
    )

@pytest.mark.parametrize('holder,search,expected', (
    ['*', {},
        itertools.product(('h1', '*'), ('n1', 'n2'), ('r1', 'r2'), ('o1', 'o2'), ('rn',))],
    [None, {},
        itertools.product(('h1', '*'), ('n1', 'n2'), ('r1', 'r2'), ('o1', 'o2'), ('rn',))],
    ['*', {'holders': ['h1']},
        itertools.product(('h1',), ('n1', 'n2'), ('r1', 'r2'), ('o1', 'o2'), ('rn',))],
    [None, {'holders': ['h1'], 'namespace': 'n2', 'objects': ['o1']},
        itertools.product(('h1',), ('n2',), ('r1', 'r2'), ('o1',), ('rn',))],
    ['*', {'holders': ['h1'], 'namespace': 'n2', 'rights': ['r2'], 'objects': ['o1']},
        [('h1', 'n2', 'r2', 'o1', 'rn')]],
))
def test_permission_grant(expected, holder, permission, search):
    for args in itertools.product(('h1', holder), ('n1', 'n2'), ('r1', 'r2'), ('o1', 'o2'), ('rn',)):
        permission.grant(*args)
    result = sepiida.permissions.search(**search)
    expected_results = [{
        'created_by'    : 'unknown',
        'holder'        : e[0],
        'namespace'     : e[1],
        'resource_name' : e[4],
        'right'         : e[2],
        'object'        : e[3],
    } for e in expected]
    for r in result:
        del r['uri']
    for exp in expected_results:
        assert exp in result
    assert len(expected_results) == len(result)
    assert permission.counters.LIST == 1

@pytest.mark.parametrize('to_delete, expected', [
    ({                    'holders': ['h1', 'h2']                                             }, 8),
    ({'objects': ['o2'],  'holders': ['h3'],                                                  }, 20),
    ({                                        'rights': ['r1', 'r2'],     'namespace': 'n1'   }, 12),
    ({'objects': ['o1'],  'holders': ['h2'],  'rights': ['r2']                                }, 22),
    ({'objects': ['o1'],  'holders': ['h2'],  'rights': ['r2'],           'namespace': 'n1'   }, 23),
])
def test_permission_captures_deletes(expected, permission, to_delete):
    for args in itertools.product(('o1', 'o2'), ('h1', 'h2', 'h3'), ('r1', 'r2'), ('n1', 'n2'), ('rn1',)):
        sepiida.permissions.create(*args)
    assert len(permission.grants) == 24
    results = sepiida.permissions.search()
    assert len(results) == 24
    sepiida.permissions.delete_all(**to_delete)
    results = sepiida.permissions.search()
    assert len(results) == expected
    assert len(permission.deletions) == 24 - expected
    assert permission.counters.DELETE == 24 - expected

def test_permission_get_public_object(app, json_client_session, permission):
    PAO_ROOT = sepiida.permissions.CONFIG['PAO_ROOT']
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        ERRORS = [
            sepiida.errors.Specification(sepiida.permissions.PermissionsNotFoundError, 404),
        ]
        @staticmethod
        def get(uuid):
            permission = '{}/permissions/{}/'.format(PAO_ROOT, uuid)
            return sepiida.permissions.get(permission)

    TestEndpoint.add_resource(app)

    permission_uuid = permission.grant(
        holder          = None,
        object_         = 'nothing',
        resource_name   = 'test-resource',
        right           = 'all',
        namespace       = 'test',
    )
    response = json_client_session.get('/test-endpoint/{}/'.format(permission_uuid.split('/')[-2]))
    assert response.status_code == 200
    assert permission.counters.GET == 1

def test_permission_get_unauthorized(app, json_client_session, permission):
    PAO_ROOT = sepiida.permissions.CONFIG['PAO_ROOT']
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        ERRORS = [
            sepiida.errors.Specification(sepiida.permissions.PermissionsNotFoundError, 404),
        ]
        @staticmethod
        def get(uuid):
            permission = '{}/permissions/{}/'.format(PAO_ROOT, uuid)
            return sepiida.permissions.get(permission)

    TestEndpoint.add_resource(app)

    user_uri = '{}/users/{}'.format(PAO_ROOT, uuid4())
    permission_uuid = permission.grant(
        holder          = user_uri,
        object_         = 'nothing',
        resource_name   = 'test-resource',
        right           = 'all',
        namespace       = 'test',
    )
    response = json_client_session.get('/test-endpoint/{}/'.format(permission_uuid.split('/')[-2]))
    assert response.status_code == 404

@pytest.mark.parametrize('session_values, uses_compression', [
    ({'a': 100*'b'}, True),
    ({'a': 'b'}, False),
])
def test_large_session_decoding(app, client, session_values, uses_compression):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            for k, v in session_values.items():
                session[k] = v
            return []
        @staticmethod
        def post():
            cookie = request.headers['Cookie']
            if cookie.startswith('session=.') != uses_compression:
                return None, 400, {}
            session_data = sepiida.fixtures.get_session_data(current_app, request)
            for k, v in session.items():
                if session_values[k] != v or session_data[k] != v:
                    return None, 400, {}
            return None, 204, {}

    TestEndpoint.add_resource(app)

    # First we set up a bunch our values in the session
    response = client.get('/test-endpoint/')
    assert response.status_code == 200
    # Then we confirm those values are present in the session
    response = client.post('/test-endpoint/')
    assert response.status_code == 204

def test_session_decoding_threading(app, json_client_session, permission, user):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            return sepiida.permissions.search(rights=['my-right'], namespace='my-namespace')

    TestEndpoint.add_resource(app)
    obj = str(uuid4())
    uri = permission.grant(user['uri'], 'my-namespace', 'my-right', obj, 'test-resource')
    response = json_client_session.get('/test-endpoint/')
    assert response.status_code == 200
    expected = [{
        'created_by'    : 'unknown',
        'holder'        : user['uri'],
        'namespace'     : 'my-namespace',
        'object'        : obj,
        'resource_name' : 'test-resource',
        'right'         : 'my-right',
        'uri'           : uri,
    }]
    assert response.json() == expected

def test_make_session(app, make_session):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            return make_response(session['user_uri'])

    TestEndpoint.add_resource(app)

    user1 = 'https://users.service/user/{}/'.format(uuid4())
    user2 = 'https://users.service/user/{}/'.format(uuid4())

    with make_session(user1) as session1:
        response = session1.get('/test-endpoint/')
        assert response.status_code == 200
        assert response.data.decode() == user1

    with make_session(user2) as session2:
        response = session2.get('/test-endpoint/')
        assert response.status_code == 200
        assert response.data.decode() == user2

def test_storage_get(storage):
    k = uuid4()
    storage.store(
        key         = k,
        bucket      = 'test',
        content     = 'some content',
        mimetype    = 'test/type',
    )
    with sepiida.storage.get(k) as x:
        content = x.read() # pylint: disable=no-member
        assert content == b'some content'

def test_storage_overwrite(storage):
    "Test that we can directly put an object in storage and then overwrite it by key"
    k = uuid4()
    storage.store(
        key         = k,
        bucket      = 'test',
        content     = 'some content',
        mimetype    = 'test/type',
    )
    storage.store(
        key         = k,
        bucket      = 'test',
        content     = 'different content',
        mimetype    = 'test/type',
    )
    assert len(storage.files) == 1
    with sepiida.storage.get(k) as x:
        content = x.read() # pylint: disable=no-member
        assert content == b'different content'

def test_storage_list(settings, storage):
    keys = [str(uuid4()) for _ in range(3)]
    for k in keys:
        storage.store(
            key         = k,
            bucket      = 'test',
            content     = 'some content',
            mimetype    = 'test/type',
        )
    results = sepiida.storage.get_files(keys)
    assert set(results.keys()) == set(keys)
    location = results[keys[0]]['uri']
    response = requests.get(location)
    assert response.status_code == 200
    data = response.json()
    assert data.pop('content').startswith(settings.STORAGE_SERVICE)
    assert data.pop('upload-location').startswith(settings.STORAGE_SERVICE)
    expected = {
        'bucket'    : 'test',
        'key'       : keys[0],
        'mimetype'  : 'test/type',
        'uri'       : location,
    }
    assert data == expected

@pytest.mark.parametrize('compress', [True])
@pytest.mark.parametrize('content', [io.BytesIO(b'some data'), b'some data'])
def test_storage_put(compress, content, storage):
    k = uuid4()
    sepiida.storage.put(k, 'test-bucket', content, 'application/octet-stream', compress=compress)
    result = storage.get(k)
    content = result.pop('content')
    upload = result.pop('upload-location')
    assert result.pop('uri')
    assert result == {
        'bucket'    : 'test-bucket',
        'key'       : str(k),
        'mimetype'  : 'application/octet-stream' if not compress else 'application/zlib.application/octet-stream',
    }
    response = requests.put(upload, data=b'other data')
    assert response.status_code == 204
    response = requests.get(content)
    assert response.status_code == 200
    assert response.content == b'other data'

def test_storage_links(settings, storage):
    k = uuid4()
    uuid = storage.store(
        key         = k,
        bucket      = 'test',
        content     = 'some content',
        mimetype    = 'test/type',
    )
    result = sepiida.storage.download_link(k)
    assert result.startswith(settings.STORAGE_SERVICE)
    assert uuid in result

    result = sepiida.storage.upload_link(k, 'test')
    assert result.startswith(settings.STORAGE_SERVICE)
    assert uuid in result

def test_session_reuse_disabled(app, client, disable_session_reuse):
    @app.route('/test-path/', methods=['GET'])
    def _handle_get(): #pylint: disable=unused-variable
        session1 = sepiida.requests.user_session(session='nothing')
        session2 = sepiida.requests.user_session(session='nothing')
        return '', 200 if id(session1) != id(session2) else 400
    sepiida.requests.add_request_handler(app)
    with disable_session_reuse():
        response = client.get('/test-path/')
    assert response.status_code == 200
    # Because of the nature of this test it will leak state badly unless we clean up after it
    sepiida.requests.remove_request_handler()

# Use parametrize to check for state leaks between runs
@pytest.mark.parametrize('newvalue', (None, 'foo', None))
def test_fakeenv(fakeenv, newvalue):
    if newvalue is None:
        assert fakeenv['PWD'].startswith('/')
    else:
        fakeenv['PWD'] = newvalue
        assert os.environ['PWD'] == newvalue
