import itertools
import json
import time
import urllib.parse
import uuid

import pytest

import sepiida.endpoints
import sepiida.permissions


@pytest.fixture
def permissions_payload():
    return {
        'holder'        : 'https://users.service/123/',
        'namespace'     : 'hoth',
        'object'        : 'https://models.service/123/',
        'resource_name' : 'thingy',
        'right'         : 'all',
    }

def permissions_call():
    return {
        'holder'        : 'https://users.service/123/',
        'namespace'     : 'hoth',
        'object_'       : 'https://models.service/123/',
        'resource_name' : 'thingy',
        'right'         : 'all',
    }

@pytest.mark.usefixtures("app")
@pytest.mark.parametrize('status, error', (
    (400, sepiida.permissions.PermissionsBadRequestError),
    (401, sepiida.permissions.PermissionsUnauthorizedError),
    (403, sepiida.permissions.PermissionsForbiddenError),
    (404, sepiida.permissions.PermissionsNotFoundError),
    (500, sepiida.permissions.PermissionsRequestError),
    (503, sepiida.permissions.PermissionsRequestError),
))
def test_send_request_failure(httpretty, status, error):
    sepiida.permissions.configure('permissions.tests', 'https://pao.service')
    http_method = 'GET'
    uri = 'https://pao.service/123/'
    httpretty.register_uri(
        httpretty.GET,
        uri,
        status=status,
    )
    with pytest.raises(error) as e:
        sepiida.permissions._send_request(http_method, uri) #pylint: disable=protected-access

    response = {
        'message': 'HTTPretty :)'
    }
    expected =  f'Failed Request, Response: {status}, {response["message"]}, Payload: None'
    assert e.value.args[0] ==  expected


@pytest.mark.usefixtures("app")
def test_send_request_timeout(httpretty):
    sepiida.permissions.configure('permissions.tests', 'https://pao.service')
    def request_callback(request, uri, headers): #pylint: disable=unused-argument
        time.sleep(1)
        return (200, {}, '{}')

    http_method = 'GET'
    uri = 'https://pao.service/123/'
    httpretty.register_uri(
        httpretty.GET,
        uri,
        body=request_callback,
    )
    with pytest.raises(sepiida.permissions.PermissionsConnectionError) as e:
        sepiida.permissions._send_request(http_method, uri, timeout=0.01) #pylint: disable=protected-access
    assert 'Read timed out' in str(e.value)

@pytest.mark.usefixtures("app")
def test_send_request_pao_root():
    sepiida.permissions.configure('permissions.tests', None)
    # the reasone behind it is to reset the state so there aren't any leaks throughout our tests
    http_method = 'GET'
    with pytest.raises(sepiida.permissions.PermissionsPaoRootError):
        sepiida.permissions._send_request(http_method) # pylint: disable=protected-access

@pytest.mark.usefixtures("app")
def test_send_request_privileged(basic_auth, httpretty, settings):
    def validate_body(request, _, headers):
        success = request.headers['Authorization'] == basic_auth('api', settings.API_TOKEN)
        return 200, headers, json.dumps({'success': success})
    httpretty.register_uri(httpretty.POST,
        urllib.parse.urljoin(settings.USER_SERVICE, '/permissions/'),
        body=validate_body,
        status_code=201,
    )
    response = sepiida.permissions._send_request('POST', payload={}, privileged=True) # pylint: disable=protected-access
    assert response.ok
    assert response.json() == {'success': True}

@pytest.mark.usefixtures("app")
def test_send_request_privileged_decorator(basic_auth, httpretty, settings):
    def validate_body(request, _, headers):
        success = request.headers['Authorization'] == basic_auth('api', settings.API_TOKEN)
        return 200, headers, json.dumps({'success': success})
    httpretty.register_uri(httpretty.POST,
        urllib.parse.urljoin(settings.USER_SERVICE, '/permissions/'),
        body=validate_body,
        status_code=201,
    )
    with sepiida.permissions.always_privileged():
        response = sepiida.permissions._send_request('POST', payload={}, privileged=False) # pylint: disable=protected-access
    assert response.ok
    assert response.json() == {'success': True}

    response = sepiida.permissions._send_request('POST', payload={}, privileged=False) # pylint: disable=protected-access
    assert response.ok
    assert response.json() == {'success': False}

@pytest.mark.usefixtures('app')
def test_send_request_includes_origin(httpretty):
    def validate_body(request, _, headers):
        success = request.headers['Origin'] == 'geocities.com'
        return 200, headers, json.dumps({'success': success})
    sepiida.permissions.configure('geocities.com', 'https://users.service')
    httpretty.register_uri(httpretty.POST,
        'https://users.service/permissions/',
        body=validate_body,
        status_code=201,
    )
    response = sepiida.permissions._send_request('POST', payload={}, privileged=True) # pylint: disable=protected-access
    assert response.ok
    assert response.json() == {'success': True}

@pytest.mark.usefixtures("app")
@pytest.mark.parametrize('permissions, expected', (
    (['herp'], True),
    ([], False),
))
def test_has_right(mocker, permissions, expected):
    resource = ''
    right = ''
    namespace = ''
    permissions_count = mocker.patch('sepiida.permissions.search', return_value=permissions)

    has_right = sepiida.permissions.has_right(resource, right, namespace)
    assert permissions_count.called_once_with(resource, right, namespace)
    assert has_right == expected

@pytest.mark.usefixtures("app", 'permission')
@pytest.mark.parametrize('kwargs, expected', [
    ({'object_'  : 'o3'},    False),
    ({'object_'  : 'o2'},    True),
    ({'object_'  : 'o2', 'holder': 'h4'},    False),
    ({'object_'  : 'o2', 'holder': 'h1'},    True),
    ({'right'    : 'r3', 'holder': 'h1'},    False),
    ({'right'    : 'r2', 'holder': 'h1', 'namespace': 'n2'},    True),
])
def test_has_any(kwargs, expected):
    for args in itertools.product(('o1', 'o2'), ('h1', 'h2', 'h3'), ('r1', 'r2'), ('n1', 'n2'), ('rn1', 'rn2')):
        sepiida.permissions.create(*args)
    has_any = sepiida.permissions.has_any(**kwargs)
    assert has_any == expected

@pytest.mark.usefixtures("app")
@pytest.mark.parametrize('permissions', (
    ([{ 'uri': 'herp' }]),
    ([{ 'uri': 'herp' }, { 'uri' : 'derp' }]),
))
def test_delete_all(mocker, permissions):
    search_mock = mocker.patch('sepiida.permissions.search', return_value=permissions)
    delete_mock = mocker.patch('sepiida.permissions.delete')

    sepiida.permissions.delete_all(
        holders     = None,
        objects     = None,
        rights      = None,
        namespace   = None,
    )

    search_mock.assert_called_once_with(
        holders     = None,
        objects     = None,
        rights      = None,
        namespace   = None,
    )
    assert delete_mock.call_count == len(permissions)

@pytest.mark.usefixtures("app")
@pytest.mark.parametrize('url, objects, holders, rights, namespace', (
    ('filter[object]=resource_proxy', ['resource_proxy'], None, None, None),
    ('filter[holder]=holder_proxy', None, ['holder_proxy'], None, None),
    ('filter[right]=right_proxy', None, None, ['right_proxy'], None),
    ('filter[namespace]=namespace_proxy', None, None, None, ['namespace_proxy']),
))
def test_search(httpretty, permissions_payload, url, objects, holders, rights, namespace): #pylint: disable=redefined-outer-name
    sepiida.permissions.configure('permissions.tests', 'https://pao.service')
    url = 'https://pao.service/permissions/?{}'.format(url)
    httpretty.register_uri(
        httpretty.GET,
        url,
        body=json.dumps({'resources' : [permissions_payload]}),
        status=200,
    )
    response = sepiida.permissions.search(objects=objects, holders=holders, rights=rights, namespace=namespace)
    assert response == [permissions_payload]

@pytest.mark.usefixtures("app")
def test_get(httpretty, permissions_payload): #pylint: disable=redefined-outer-name
    sepiida.permissions.configure('permissions.tests', 'https://pao.service')
    uri = 'https://pao.service/123/'
    httpretty.register_uri(
        httpretty.GET,
        uri,
        body=json.dumps(permissions_payload),
        status=200,
    )
    response = sepiida.permissions.get(uri)
    assert response == permissions_payload

def _setup_request_callback(httpretty, method, uri, payload=None, location=None):
    def request_callback(request, _, headers):
        status = 204
        if payload:
            status = 204 if json.loads(request.body.decode()) == permissions_payload() else 400
        headers['Location'] = location or '123'
        return (status, headers, '')

    httpretty.register_uri(
        method,
        uri,
        body=request_callback,
    )

@pytest.mark.usefixtures("app")
def test_create(httpretty, permissions_payload): #pylint: disable=redefined-outer-name
    sepiida.permissions.configure('permissions.tests', 'https://pao.service')
    uri = 'https://pao.service/permissions/'
    location = 'https://pao.service'
    _setup_request_callback(
        httpretty,
        method=httpretty.POST,
        uri=uri,
        payload=permissions_payload,
        location=location
    )

    response = sepiida.permissions.create(**permissions_call())

    assert response == location

@pytest.mark.usefixtures("app")
def test_update(httpretty, permissions_payload): #pylint: disable=redefined-outer-name
    sepiida.permissions.configure('permissions.tests', 'https://pao.service')
    uri = 'https://pao.service/123/'
    location ='https://pao.service'
    _setup_request_callback(
        httpretty,
        method=httpretty.PUT,
        uri=uri,
        payload=permissions_payload,
        location=location
    )

    sepiida.permissions.update(uri, **permissions_call())

    assert httpretty.last_request().method == 'PUT'
    assert httpretty.last_request().path == '/123/'
    assert json.loads(httpretty.last_request().body.decode()) == permissions_payload

@pytest.mark.usefixtures("app")
def test_delete(httpretty): #pylint: disable=redefined-outer-name
    sepiida.permissions.configure('permissions.tests', 'https://pao.service')
    uri = 'https://pao.service/123/'
    location = 'https://pao.service'
    _setup_request_callback(httpretty,
        method=httpretty.DELETE,
        uri=uri,
        location=location
    )
    sepiida.permissions.delete(uri)
    assert httpretty.last_request().method == 'DELETE'
    assert httpretty.last_request().path == '/123/'

def _uuid(i):
    "0 -> 00000000-0000-0000-0000-000000000000"
    return uuid.UUID(str(i)*8 + '-' + ((str(i)*4 + '-') * 3) + str(i)*12)

def _uri(i):
    "0 -> https://users.service/permissions/00000000-0000-0000-0000-000000000000/"
    u = _uuid(i)
    return sepiida.routing.uri('test-resource', u)

def _add_test_endpoint(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def get():
            return {}
    TestEndpoint.add_resource(app, 'test-resource')

@pytest.mark.parametrize('filters,grants,expected', (
    [[],        [0],    [0]],
    [[0],       [1],    []],
    [[0],       [0, 1], [0]],
    [[0, 1],    [0],    [0]],
))
def test_update_filters(app, expected, filters, grants, permission, user):
    _add_test_endpoint(app)
    for obj in grants:
        permission.grant(user['uri'], 'some-namespace', 'some-right', _uri(obj), 'test-resource')

    filters = {'uuid': [_uuid(f) for f in filters]}
    updated = sepiida.permissions.update_filters(filters, 'some-namespace', 'test-resource')
    expected = {'uuid': {_uuid(e) for e in expected}}
    assert updated == expected

def test_update_filters_always_privileged(app, permission, user):
    _add_test_endpoint(app)

    permission.grant(user['uri'], 'some-namespace', 'some-right', _uri(0), 'test-resource')

    filters = {'uuid': [_uuid(0), _uuid(1)]}
    sepiida.permissions.update_filters(filters, 'some-namespace', 'test-resource')
    expected = {'uuid': {_uuid(0)}}
    assert filters == expected, "control, testing that update_filters works correctly normally"

    filters = {'uuid': [_uuid(0), _uuid(1)]}
    with sepiida.permissions.always_privileged():
        sepiida.permissions.update_filters(filters, 'some-namespace', 'test-resource')
    expected = {'uuid': [_uuid(0), _uuid(1)]}
    assert filters == expected, "update_filter does not filter when always_privileged is on"

def test_update_filters_with_id(app, permission, user):
    _add_test_endpoint(app)
    permission.grant(
        holder          = user['uri'],
        namespace       = 'test-ns',
        object_         = sepiida.routing.uri('test-resource', _id=1),
        resource_name   = 'test-resource',
        right           = 'all',
    )
    permission.grant(
        holder          = user['uri'],
        namespace       = 'test-ns',
        object_         = sepiida.routing.uri('test-resource', _uuid(0)),
        resource_name   = 'test-resource',
        right           = 'all',
    )
    filters = {'uuid': []}
    sepiida.permissions.update_filters(filters, 'test-ns', 'test-resource')
    assert filters == {'uuid': {_uuid(0), 1}}

def test_update_filters_with_uuids(app, httpretty, permission, user):
    """
    Ensure that we properly request only the objects we are already going to filter against.
    IE, if we are already filtering down to a single UUID only check if we've got permission
    on that one object
    """
    _add_test_endpoint(app)
    permission.grant(user['uri'], 'some-namespace', 'some-right', _uri(1), 'test-resource')

    filters = {'uuid': [_uuid(f) for f in [0, 1]]}
    updated = sepiida.permissions.update_filters(filters, 'some-namespace', 'test-resource')
    expected = {'uuid': {_uuid(1)}}
    assert updated == expected
    last_request = httpretty.last_request()
    body = last_request.body.decode('utf-8')
    assert body == ('filter[object]=https://sepiida.service/test-endpoint/00000000-0000-0000-0000-000000000000/,'
        'https://sepiida.service/test-endpoint/11111111-1111-1111-1111-111111111111/&'
        'filter[resource_name]=test-resource&filter[namespace]=some-namespace&fields=[resources.object]')

def test_always_privileged_nested():
    with sepiida.permissions.always_privileged():
        assert sepiida.permissions.is_privileged()
        with sepiida.permissions.always_privileged():
            assert sepiida.permissions.is_privileged()
        assert sepiida.permissions.is_privileged()


def test_always_privileged_does_not_leak():
    try:
        with sepiida.permissions.always_privileged():
            raise Exception("Bad things")
    except Exception: # pylint: disable=broad-except
        pass # nothing to do here, we just wanted an exception
    finally:
        assert not sepiida.permissions.is_privileged()
