import flask
import uuid

import sepiida.environment
import sepiida.sentry


def test_add_support():
    user = {'uri': 'https://users.service/users/{}/'.format(uuid.uuid4())}
    # Most of our clients will not have an app context at the time they call this
    # function, so let's make sure that we test that way. This is why we have to build
    # the app ourselves right here - otherwise our test system creates an app context
    assert not flask.has_app_context()
    settings = sepiida.environment.parse(sepiida.environment.DEFAULT_SPEC)
    app = flask.Flask('sepiida')
    app.config['SECRET_KEY'] = settings.SECRET_KEY

    @app.route('/test-endpoint/')
    def test(): # pylint: disable=unused-variable
        assert flask.current_app.config['RAVEN'].client.context['user']['uri'] == user['uri']
        assert flask.current_app.config['RAVEN'].client.release == '1.0'
        return flask.make_response('')

    sepiida.session.register_session_handlers(app)
    sentry_dsn = 'https://a:b@app.getsentry.com/35300'
    sepiida.sentry.add_sentry_support(app, sentry_dsn, '1.0')
    with app.test_client() as test_client:
        with test_client.session_transaction() as session:
            session['user_uri'] = user['uri']
        response = test_client.get('/test-endpoint/')
        assert response.status_code == 200

def test_handle_reverse_proxy():
    app = flask.Flask('sepiida')
    sepiida.sentry.handle_reverse_proxy(app)
    assert isinstance(app.wsgi_app, sepiida.sentry.ProxyFix)
