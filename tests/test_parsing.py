import pytest

import sepiida.parsing


@pytest.mark.parametrize('queryarg, values, message', (
    ('filter',      [], "You requested a filter with no name. Filter requests should be of the form 'filter[property]'"),
    ('filter[foo]', [], "You requested a filter with no values. This won't match anything, ever"),
    ('filter[',     [], "You requested a filter 'filter['. Filter requests should be of the form 'filter[property]'"),
    ('filter]',     [], "You requested a filter 'filter]'. Filter requests should be of the form 'filter[property]'"),
    ('filter[]',    [], "You requested a filter 'filter[]' with no name. Filter requests should be of the form 'filter[property]'"),
    ('filter[foo]<y',['x'], (
        "You requested a filter 'filter[foo]<y=x' with an equal in the value. "
        "Filter requests should be of the form 'filter[property]=value'")),
    ('filter[foo]<2015-01-01',['2015-01-02'], (
        "You requested a filter 'filter[foo]<2015-01-01=2015-01-02' with an equal in the value. "
        "Filter requests should be of the form 'filter[property]=value'")),
    ('filter[foo]>y',['x'], (
        "You requested a filter 'filter[foo]>y=x' with an equal in the value. "
        "Filter requests should be of the form 'filter[property]=value'")),
    ('filter[foo]>2015-01-01',['2015-01-02'], (
        "You requested a filter 'filter[foo]>2015-01-01=2015-01-02' with an equal in the value. "
        "Filter requests should be of the form 'filter[property]=value'")),
    ('filter[foo]>y>b',[], "You requested a filter 'filter[foo]>y>b'. Filter requests should be of the form 'filter[property]'"),
    ('filter[foo]>2015-01-02>2015-01-01',[], (
        "You requested a filter 'filter[foo]>2015-01-02>2015-01-01'. Filter requests should be of the form 'filter[property]'")),
))
def test_filter_parse_failure(message, queryarg, values):
    with pytest.raises(sepiida.parsing.ParseError) as e:
        sepiida.parsing.FilterArgument(queryarg, values)
    assert str(e.value) == message

@pytest.mark.parametrize('queryarg, values, operation, expected', (
    ('filter[foo]', ['bar'], sepiida.parsing.FilterArgument.EQ, ['bar']),
    ('filter[foo]', ['2015-01-01'], sepiida.parsing.FilterArgument.EQ, ['2015-01-01']),
    ('filter[foo]<', ['bar'], sepiida.parsing.FilterArgument.LTE, ['bar']),
    ('filter[foo]<', ['2015-01-01'], sepiida.parsing.FilterArgument.LTE, ['2015-01-01']),
    ('filter[foo]>', ['bar'], sepiida.parsing.FilterArgument.GTE, ['bar']),
    ('filter[foo]>', ['2015-01-01'], sepiida.parsing.FilterArgument.GTE, ['2015-01-01']),
    ('filter[foo]>bar', [], sepiida.parsing.FilterArgument.GT, ['bar']),
    ('filter[foo]>2015-01-01', [], sepiida.parsing.FilterArgument.GT, ['2015-01-01']),
    ('filter[foo]<bar', [], sepiida.parsing.FilterArgument.LT, ['bar']),
    ('filter[foo]<2015-01-01', [], sepiida.parsing.FilterArgument.LT, ['2015-01-01']),
))
def test_filter_parse(operation, queryarg, values, expected):
    f = sepiida.parsing.FilterArgument(queryarg, values)
    assert f.values == expected
    assert f.name == 'foo'
    assert f.operation == operation

@pytest.mark.parametrize('values, expected', (
    (['bar,baz'], ['bar', 'baz']),
    (['bar ,baz'], ['bar ', 'baz']),
    (['bar,baz '], ['bar', 'baz ']),
    (['bar,baz,'], ['bar', 'baz', '']),
))
def test_standard_filter_parse_multiple(expected, values):
    f = sepiida.parsing.FilterArgument('filter[foo]', values)
    assert f.values == expected

@pytest.mark.parametrize('queryarg, expected', (
    ('filter[foo]>bar,baz', ['bar', 'baz']),
    ('filter[foo]>2015-01-01,2016-01-01', ['2015-01-01', '2016-01-01']),
    ('filter[foo]<bar ,baz', ['bar ', 'baz']),
    ('filter[foo]<2015-01-01 ,2016-01-01', ['2015-01-01 ', '2016-01-01']),
))
def test_nonstandard_filter_parse_multiple(expected, queryarg):
    f = sepiida.parsing.FilterArgument(queryarg, [])
    assert f.values == expected

@pytest.mark.parametrize('queryarg, values, message', (
    ('sort',  [], "You requested a sort with no values. You must provide at least one sort value."),
    ('sort[', [], "You requested a sort 'sort['. Sort requests should be of the form 'sort'"),
    ('sort]', [], "You requested a sort 'sort]'. Sort requests should be of the form 'sort'"),
    ('sort',  ['test, -test'], "You requested a sort of the same field multiple times. You may only sort each field once."),
))
def test_sort_parse_failure(message, queryarg, values):
    with pytest.raises(sepiida.parsing.ParseError) as e:
        sepiida.parsing.SortArgument(queryarg, values)
    assert str(e.value) == message

@pytest.mark.parametrize('queryarg, values, expected', (
    ('sort', ['foo'], [{'name': 'foo', 'ascending': True}]),
    ('sort', ['foo,bar'], [{'name': 'foo', 'ascending': True}, {'name': 'bar', 'ascending': True}]),
    ('sort', ['foo, bar'], [{'name': 'foo', 'ascending': True}, {'name': 'bar', 'ascending': True}]),
    ('sort', ['-foo, bar'], [{'name': 'foo', 'ascending': False}, {'name': 'bar', 'ascending': True}]),
))
def test_sort_parse(queryarg, values, expected):
    f = sepiida.parsing.SortArgument(queryarg, values)
    assert f.values == expected


@pytest.mark.parametrize('queryarg, values, message', (
    ('search',      [], "You requested a search with no name. Search requests should be of the form 'search[property]'"),
    ('search[foo]', [], "You requested a search with no values. This won't match anything, ever"),
    ('search[',     [], "You requested a search 'search['. Search requests should be of the form 'search[property]'"),
    ('search]',     [], "You requested a search 'search]'. Search requests should be of the form 'search[property]'"),
    ('search[]',    [], "You requested a search 'search[]' with no name. Search requests should be of the form 'search[property]'"),
))
def test_search_parse_failure(message, queryarg, values):
    with pytest.raises(sepiida.parsing.ParseError) as e:
        sepiida.parsing.SearchArgument(queryarg, values)
    assert str(e.value) == message

@pytest.mark.parametrize('queryarg, values, operation, expected', (
    ('search[foo]', ['bar'], sepiida.parsing.FilterArgument.EQ, ['bar']),
    ('search[foo]', ['2015-01-01'], sepiida.parsing.FilterArgument.EQ, ['2015-01-01']),
))
def test_search_parse(operation, queryarg, values, expected):
    f = sepiida.parsing.SearchArgument(queryarg, values)
    assert f.values == expected
    assert f.name == 'foo'
    assert f.operation == operation

@pytest.mark.parametrize('values, expected', (
    (['bar,baz'], ['bar', 'baz']),
    (['bar ,baz'], ['bar ', 'baz']),
    (['bar,baz '], ['bar', 'baz ']),
    (['bar,baz,'], ['bar', 'baz', '']),
))
def test_standard_search_parse_multiple(expected, values):
    f = sepiida.parsing.SearchArgument('search[foo]', values)
    assert f.values == expected
