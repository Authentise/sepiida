# pylint: disable=too-many-lines
import functools
from datetime import date, datetime, timedelta, timezone
from uuid import UUID, uuid4

import flask
import pytest
from dateutil.tz import tzutc

import sepiida.endpoints
import sepiida.fields
import tests.helpers


class SomeThing:
    def __str__(self):
        return "something"

def _create_unreachable_endpoint(app, signature):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = signature
        @staticmethod
        def list():
            assert False, 'should never reach user code'
        @staticmethod
        def get():
            assert False, 'should never reach user code'

    TestEndpoint.add_resource(app, 'test-endpoint')

@pytest.mark.parametrize('field, payload, result', [
    (sepiida.fields.String(),   {'my_prop': None},                               {'my_prop': None}),
    (sepiida.fields.String(),   {'my_prop': ''},                                 {'my_prop': ''}),
    (sepiida.fields.String(),   {'my_prop': 'my_value'},                         {'my_prop': 'my_value'}),
    (sepiida.fields.String(),   {'my_prop': 1},                                  {'my_prop': '1'}),
    (sepiida.fields.String(),   {'my_prop': 2.2},                                {'my_prop': '2.2'}),
    (sepiida.fields.String(),   {'my_prop': SomeThing()},                        {'my_prop': 'something'}),
    (sepiida.fields.Integer(),  {'my_prop': None},                               {'my_prop': None}),
    (sepiida.fields.Integer(),  {'my_prop': 1},                                  {'my_prop': 1}),
    (sepiida.fields.Integer(),  {'my_prop': 1.1},                                {'my_prop': 1}),
    (sepiida.fields.Integer(),  {'my_prop': 1.9},                                {'my_prop': 1}),
    (sepiida.fields.Integer(),  {'my_prop': '2'},                                {'my_prop': 2}),
    (sepiida.fields.Float(),    {'my_prop': None},                               {'my_prop': None}),
    (sepiida.fields.Float(),    {'my_prop': 1},                                  {'my_prop': 1.0}),
    (sepiida.fields.Float(),    {'my_prop': 1.1},                                {'my_prop': 1.1}),
    (sepiida.fields.Float(),    {'my_prop': '2'},                                {'my_prop': 2.0}),
    (sepiida.fields.Float(),    {'my_prop': '2.1'},                              {'my_prop': 2.1}),
    (sepiida.fields.Boolean(),  {'my_prop': None},                               {'my_prop': None}),
    (sepiida.fields.Boolean(),  {'my_prop': 0},                                  {'my_prop': False}),
    (sepiida.fields.Boolean(),  {'my_prop': 1},                                  {'my_prop': True}),
    (sepiida.fields.Boolean(),  {'my_prop': 1.1},                                {'my_prop': True}),
    (sepiida.fields.Boolean(),  {'my_prop': '2'},                                {'my_prop': True}),
    (sepiida.fields.Boolean(),  {'my_prop': 'bob'},                              {'my_prop': True}),
    (sepiida.fields.Boolean(),  {'my_prop': ''},                                 {'my_prop': False}),
    (sepiida.fields.Datetime(),
        {'my_prop': datetime(2001, 11, 9, 1, 8, 47, 123456)},
        {'my_prop': '2001-11-09T01:08:47.123456+00:00'},
    ),
    (sepiida.fields.Datetime(),
        {'my_prop': datetime(2001, 11, 9, 1, 8, 47, 0)},
        {'my_prop': '2001-11-09T01:08:47+00:00'},
    ),
    (sepiida.fields.Datetime(),
        {'my_prop': datetime(2001, 11, 9, 1, 8, 47)},
        {'my_prop': '2001-11-09T01:08:47+00:00'},
    ),
    (sepiida.fields.Datetime(),
        {'my_prop': datetime(2001, 11, 9, 1, 8, 47, tzinfo=timezone(timedelta(hours=3)))},
        {'my_prop': '2001-11-08T22:08:47+00:00'},
    ),
    (sepiida.fields.Datetime(), {'my_prop': None},                               {'my_prop': None}),
    (sepiida.fields.Date(),     {'my_prop': date(2001, 11, 9)},                  {'my_prop': '2001-11-09'}),
    (sepiida.fields.Date(),     {'my_prop': None},                               {'my_prop': None}),
    (sepiida.fields.UUID(),     {'my_prop': UUID('00a1774d-03cc-45d2-af9c-fb308470f832')},
                                {'my_prop': '00a1774d-03cc-45d2-af9c-fb308470f832'}),
    (sepiida.fields.UUID(),     {'my_prop': None},                               {'my_prop': None}),
    (sepiida.fields.URI('test_endpoint'),
                                {'my_prop': 'https://sepiida.service/test-endpoint/0/'},
                                {'my_prop': 'https://sepiida.service/test-endpoint/0/'}),
    (sepiida.fields.URL(),      {'my_prop': 'https://some.uri'},
                                {'my_prop': 'https://some.uri'}),
])
def test_get_string_conversion(app, field, json_client, payload, result):
    signature = sepiida.fields.Object(s={
        'my_prop': field
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, payload)
    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == result

@pytest.mark.parametrize('payload, result', [
    (['a', 1, False],       ['a', 1, False]),
    ([None],                [None]),
    ([],                    []),
])
def test_get_list_conversion_no_signature(app, json_client, payload, result):
    tests.helpers.create_expected_payload_endpoint(app, sepiida.fields.Array(), payload)
    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == result

@pytest.mark.parametrize('payload, result', [
    (['a', 1, False],       ['a', '1', 'False']),
    ([None],                [None]),
    ([],                    []),
])
def test_get_list_conversion_signature(app, json_client, payload, result):
    tests.helpers.create_expected_payload_endpoint(app, sepiida.fields.Array(s=sepiida.fields.String()), payload)
    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == result

@pytest.mark.parametrize('field, value, error', [
    (sepiida.fields.Float(), 'baz', "Programmer failed to provide the correct type for foo:"),
    (sepiida.fields.Float(), {}, "Programmer failed to provide the correct type for foo:"),
    (sepiida.fields.Float(), float('NaN'), ("Programmer failed to provide the correct type for foo: "
                                            "Programmer provided 'nan' which cannot be serialized correctly")),
    (sepiida.fields.Float(), float('inf'), ("Programmer failed to provide the correct type for foo: "
                                            "Programmer provided 'inf' which cannot be serialized correctly")),
    (sepiida.fields.Float(), float('-inf'), ("Programmer failed to provide the correct type for foo: "
                                            "Programmer provided '-inf' which cannot be serialized correctly")),
    (sepiida.fields.Integer(), 'baz', "Programmer failed to provide the correct type for foo:"),
    (sepiida.fields.Datetime(), 'baz', "Programmer failed to provide the correct type for foo: baz is not a valid datetime"),
    (sepiida.fields.Date(), 'baz', "Programmer failed to provide the correct type for foo: baz is not a valid date"),
    (sepiida.fields.UUID(), 'baz', "Programmer failed to provide the correct type for foo: baz is not a valid UUID"),
])
def test_package_invalid_type(app, error, field, json_client, value):
    signature = sepiida.fields.Object(s={
        'foo'   : field,
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, {'foo': value})
    with pytest.raises(Exception) as e:
        json_client.get('/test-endpoint/')
    assert error in str(e)

@pytest.mark.parametrize('field, payload', [
    (sepiida.fields.String(),       {'my_prop' : 1}),
    (sepiida.fields.String(),       {'my_prop' : 1.1}),
    (sepiida.fields.String(),       {'my_prop' : True}),
    (sepiida.fields.String(),       {'my_prop' : {}}),
    (sepiida.fields.String(),       {'my_prop' : []}),
    (sepiida.fields.String(),       {'my_prop' : None}),
    (sepiida.fields.Integer(),      {'my_prop' : 'foo'}),
    (sepiida.fields.Integer(),      {'my_prop' : True}),
    (sepiida.fields.Integer(),      {'my_prop' : {}}),
    (sepiida.fields.Integer(),      {'my_prop' : []}),
    (sepiida.fields.Integer(),      {'my_prop' : None}),
    (sepiida.fields.Float(),        {'my_prop' : 'foo'}),
    (sepiida.fields.Float(),        {'my_prop' : True}),
    (sepiida.fields.Float(),        {'my_prop' : {}}),
    (sepiida.fields.Float(),        {'my_prop' : []}),
    (sepiida.fields.Float(),        {'my_prop' : None}),
    (sepiida.fields.Boolean(),      {'my_prop' : 1}),
    (sepiida.fields.Boolean(),      {'my_prop' : 1.1}),
    (sepiida.fields.Boolean(),      {'my_prop' : 'foo'}),
    (sepiida.fields.Boolean(),      {'my_prop' : {}}),
    (sepiida.fields.Boolean(),      {'my_prop' : []}),
    (sepiida.fields.Boolean(),      {'my_prop' : None}),
    (sepiida.fields.Array(),     {'my_prop' : 1}),
    (sepiida.fields.Array(),     {'my_prop' : 1.1}),
    (sepiida.fields.Array(),     {'my_prop' : 'foo'}),
    (sepiida.fields.Array(),     {'my_prop' : True}),
    (sepiida.fields.Array(),     {'my_prop' : {}}),
    (sepiida.fields.Array(),     {'my_prop' : None}),
    (sepiida.fields.Object(),   {'my_prop' : 1}),
    (sepiida.fields.Object(),   {'my_prop' : 1.1}),
    (sepiida.fields.Object(),   {'my_prop' : 'foo'}),
    (sepiida.fields.Object(),   {'my_prop' : True}),
    (sepiida.fields.Object(),   {'my_prop' : []}),
    (sepiida.fields.Object(),   {'my_prop' : None}),
    (sepiida.fields.Datetime(),     {'my_prop' : 1}),
    (sepiida.fields.Datetime(),     {'my_prop' : 1.1}),
    (sepiida.fields.Datetime(),     {'my_prop' : None}),
    (sepiida.fields.Datetime(),     {'my_prop' : True}),
    (sepiida.fields.Datetime(),     {'my_prop' : []}),
    (sepiida.fields.Date(),         {'my_prop' : 1}),
    (sepiida.fields.Date(),         {'my_prop' : 1.1}),
    (sepiida.fields.Date(),         {'my_prop' : None}),
    (sepiida.fields.Date(),         {'my_prop' : True}),
    (sepiida.fields.Date(),         {'my_prop' : []}),
    (sepiida.fields.UUID(),         {'my_prop' : 1}),
    (sepiida.fields.UUID(),         {'my_prop' : 1.1}),
    (sepiida.fields.UUID(),         {'my_prop' : 'foo'}),
    (sepiida.fields.UUID(),         {'my_prop' : True}),
    (sepiida.fields.UUID(),         {'my_prop' : None}),
    (sepiida.fields.URI(''),        {'my_prop' : 1}),
    (sepiida.fields.URI(''),        {'my_prop' : 1.1}),
    (sepiida.fields.URI(''),        {'my_prop' : True}),
    (sepiida.fields.URI(''),        {'my_prop' : None}),
    (sepiida.fields.URL(),          {'my_prop' : None}),
    (sepiida.fields.URL(),          {'my_prop' : 'foo'}),
    (sepiida.fields.URL(),          {'my_prop' : 'foo://'}),
    (sepiida.fields.URL(),          {'my_prop' : '/baz'}),
    (sepiida.fields.URL(),          {'my_prop' : '://baz'}),
    (sepiida.fields.URL(),          {'my_prop' : 1}),
    (sepiida.fields.URL(),          {'my_prop' : True}),
])
def test_post_put_bad_type(app, field, json_client, payload):
    signature = sepiida.fields.Object(s={
        'my_prop'   : field
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, None)

    expected_type_name = {
        sepiida.fields.String       : 'a string',
        sepiida.fields.Integer      : 'a number',
        sepiida.fields.Float        : 'a number',
        sepiida.fields.Boolean      : 'a boolean',
        sepiida.fields.Array        : 'an array',
        sepiida.fields.Object       : 'an object',
        sepiida.fields.Datetime     : 'a datetime',
        sepiida.fields.Date         : 'a date',
        sepiida.fields.UUID         : 'a uuid',
        sepiida.fields.URI          : 'a uri',
        sepiida.fields.URL          : 'a URL',
    }[type(field)]
    provided_type_name = sepiida.fields.TYPES_TO_NAME[type(payload['my_prop'])]
    message = "The property 'my_prop' must be {}. You provided {}".format(expected_type_name, provided_type_name)

    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 400
    assert response.json()['errors'][0]['title'] == message

    response = json_client.put('/test-endpoint/0/', json=payload)
    assert response.status_code == 400
    assert response.json()['errors'][0]['title'] == message

def test_post_put_bad_type_obj_multi_error(app, json_client):
    signature = sepiida.fields.Object(s={
        'prop1'     : sepiida.fields.String(),
        'prop2'     : sepiida.fields.Boolean(),
        'prop3'     : sepiida.fields.Object(s={
            'prop4' : sepiida.fields.Integer(),
            'prop5' : sepiida.fields.Array(s=sepiida.fields.Float()),
        })
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, None)
    payload = {
        'prop1'     : 1,
        'prop2'     : 2,
        'prop3'     : {
            'prop4' : 'foo',
            'prop5' : [True],
        }
    }
    expected = {
        'errors'    : [{
            'code'  : 'type-validation-error',
            'title' : "The property 'prop1' must be a string. You provided a number",
        },{
            'code'  : 'type-validation-error',
            'title' : "The property 'prop2' must be a boolean. You provided a number",
        },{
            'code'  : 'type-validation-error',
            'title' : "The property 'prop3.prop4' must be a number. You provided a string",
        },{
            'code'  : 'type-validation-error',
            'title' : "The property 'prop3.prop5.0' must be a number. You provided a boolean",
        }]
    }

    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 400
    assert response.json() == expected

    response = json_client.put('/test-endpoint/0/', json=payload)
    assert response.status_code == 400
    assert response.json() == expected

def test_post_put_bad_type_list_multi_error(app, json_client):
    signature = sepiida.fields.Array(s=sepiida.fields.String())
    tests.helpers.create_expected_payload_endpoint(app, signature, None)
    payload = [1, True, []]
    expected = {
        'errors'    : [{
            'code'  : 'type-validation-error',
            'title' : "The property '0' must be a string. You provided a number",
        },{
            'code'  : 'type-validation-error',
            'title' : "The property '1' must be a string. You provided a boolean",
        },{
            'code'  : 'type-validation-error',
            'title' : "The property '2' must be a string. You provided an array",
        }]
     }

    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 400
    assert response.json() == expected

    response = json_client.put('/test-endpoint/0/', json=payload)
    assert response.status_code == 400
    assert response.json() == expected

@pytest.mark.parametrize('field, payload, expected', [
    (sepiida.fields.String(),   {'my_prop': 'a'},     {'my_prop': 'a'}),
    (sepiida.fields.Integer(),  {'my_prop': 1},       {'my_prop': 1}),
    (sepiida.fields.Float(),    {'my_prop': 1},       {'my_prop': 1}),
    (sepiida.fields.Float(),    {'my_prop': 1.1},     {'my_prop': 1.1}),
    (sepiida.fields.Boolean(),  {'my_prop': True},    {'my_prop': True}),
    (sepiida.fields.Boolean(),  {'my_prop': False},   {'my_prop': False}),
    (sepiida.fields.Datetime(),
        {'my_prop': '2001-11-09T01:08:47.123456'},
        {'my_prop': datetime(2001, 11, 9, 1, 8, 47, 123456, tzinfo=tzutc())}
    ),
    (sepiida.fields.Datetime(),
        {'my_prop': '2001-11-09T01:08:47'},
        {'my_prop': datetime(2001, 11, 9, 1, 8, 47, tzinfo=tzutc())}
    ),
    (sepiida.fields.Datetime(),
        {'my_prop': '2001-12-19T01:08:47+00:00'},
        {'my_prop': datetime(2001, 12, 19, 1, 8, 47, tzinfo=tzutc())}
    ),
    (sepiida.fields.Datetime(),
        {'my_prop': '2001-10-01T02:08:47+03:00'},
        {'my_prop': datetime(2001, 9, 30, 23, 8, 47, tzinfo=tzutc())}
    ),
    (sepiida.fields.Datetime(),
        {'my_prop': '2001-10-01T02:08:47-07:30'},
        {'my_prop': datetime(2001, 10, 1, 9, 38, 47, tzinfo=tzutc())}
    ),
    (sepiida.fields.Date(),     {'my_prop': '2001-11-09'}, {'my_prop': date(2001, 11, 9)}),
    (sepiida.fields.UUID(),     {'my_prop': '00a1774d-03cc-45d2-af9c-fb308470f832'},
                                {'my_prop': UUID('00a1774d-03cc-45d2-af9c-fb308470f832')}),
    (sepiida.fields.URL(),      {'my_prop': 'https://some.where'},
                                {'my_prop': 'https://some.where'}),
    (sepiida.fields.URI('test_endpoint'),   {'my_prop': 'http://localhost/test-endpoint/123/'},
                                            {'my_prop': {
                                                '_id'               : 123,
                                                '_single_resource'  : True,
                                                'uri'               : 'http://localhost/test-endpoint/123/',
                                            }}),
])
def test_post_put_types(app, expected, field, json_client, payload):
    signature = sepiida.fields.Object(s={
        'my_prop'   : field
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)

    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 204

    response = json_client.put('/test-endpoint/0/', json=payload)
    assert response.status_code == 204

@pytest.mark.parametrize('field', [
    sepiida.fields.String,
    sepiida.fields.Integer,
    sepiida.fields.Float,
    sepiida.fields.Boolean,
    sepiida.fields.Object,
    sepiida.fields.Array,
    sepiida.fields.Datetime,
    sepiida.fields.Date,
    sepiida.fields.UUID,
    functools.partial(sepiida.fields.URI, ''),
    sepiida.fields.URL,
])
def test_get_allow_null(app, field, json_client):
    signature = sepiida.fields.Object(s={
        'my_prop'   : field(nullable=True)
    })
    expected = {'my_prop': None}
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [{'my_prop': None}]

@pytest.mark.parametrize('field', [
    sepiida.fields.String,
    sepiida.fields.Integer,
    sepiida.fields.Float,
    sepiida.fields.Boolean,
    sepiida.fields.Object,
    sepiida.fields.Array,
    sepiida.fields.Datetime,
    sepiida.fields.Date,
    sepiida.fields.UUID,
    functools.partial(sepiida.fields.URI, ''),
    sepiida.fields.URL,
])
def test_post_put_allow_null(app, field, json_client):
    signature = sepiida.fields.Object(s={
        'my_prop'   : field(nullable=True)
    })
    expected = {'my_prop': None}
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)

    response = json_client.post('/test-endpoint/', json=expected)
    assert response.status_code == 204

    response = json_client.put('/test-endpoint/0/', json=expected)
    assert response.status_code == 204

def _subobject_signature():
    return sepiida.fields.Object(s={
        'sub_obj'   : sepiida.fields.Object(s={
            'bar'   : sepiida.fields.String(),
        }),
    })

def _subobject_no_signature():
    return sepiida.fields.Object(s={
        'sub_obj'   : sepiida.fields.Object(),
    })

def test_post_put_subobject_no_signature_happy_path(app, json_client):
    signature = _subobject_no_signature()
    test_payload = {
        'sub_obj': {
            'bar': 'baz'
        }
    }
    tests.helpers.create_expected_payload_endpoint(app, signature, test_payload)
    response = json_client.post('/test-endpoint/', json=test_payload)
    assert response.status_code == 204
    response = json_client.put('/test-endpoint/0/', json=test_payload)
    assert response.status_code == 204

def test_post_put_subobject_signature_happy_path(app, json_client):
    signature = _subobject_signature()
    test_payload = {
        'sub_obj': {
            'bar': 'baz'
        }
    }
    tests.helpers.create_expected_payload_endpoint(app, signature, test_payload)
    response = json_client.post('/test-endpoint/', json=test_payload)
    assert response.status_code == 204
    response = json_client.put('/test-endpoint/0/', json=test_payload)
    assert response.status_code == 204

@pytest.mark.parametrize('payload, message', [
    ({'bar': 1},                "The property 'sub_obj.bar' must be a string. You provided a number"),
    ({'bar': 'a', 'baz': 1},    "You provided 'sub_obj.baz'. It is not a valid property for this resource"),
])
def test_post_put_subobject_errors(app, json_client, message, payload):
    signature = _subobject_signature()
    tests.helpers.create_expected_payload_endpoint(app, signature, None)

    response = json_client.post('/test-endpoint/', json={'sub_obj': payload})
    assert response.status_code == 400
    assert response.json()['errors'][0]['title'] == message

    response = json_client.put('/test-endpoint/0/', json={'sub_obj': payload})
    assert response.status_code == 400
    assert response.json()['errors'][0]['title'] == message

def test_post_error(app, json_client):
    signature = _subobject_signature()
    tests.helpers.create_expected_payload_endpoint(app, signature, None)

    payload = {}
    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 400
    assert response.json()['errors'][0]['title'] == "You did not provide the sub_obj property. It is required when POSTing to this resource"

def test_methods_get(app, json_client):
    signature = sepiida.fields.Object(s={
        'my_prop'   : sepiida.fields.String(methods=['PUT', 'POST']),
    })
    expected = {'my_prop': 'foo'}
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)

    response = json_client.post('/test-endpoint/', json=expected)
    assert response.status_code == 204

    response = json_client.put('/test-endpoint/0/', json=expected)
    assert response.status_code == 204

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [{}]

    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == {}

def test_methods_put(app, json_client):
    signature = sepiida.fields.Object(s={
        'my_prop'   : sepiida.fields.String(methods=['GET', 'POST']),
    })
    expected = {'my_prop': 'foo'}
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)

    response = json_client.post('/test-endpoint/', json=expected)
    assert response.status_code == 204

    response = json_client.put('/test-endpoint/0/', json=expected)
    assert response.status_code == 400
    assert response.json()['errors'][0]['title'] == "You provided 'my_prop'. It cannot be directly set via PUT"

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [expected]

    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == expected

def test_methods_post(app, json_client):
    signature = sepiida.fields.Object(s={
        'my_prop'   : sepiida.fields.String(methods=['GET', 'PUT']),
    })
    expected = {'my_prop': 'foo'}
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)

    response = json_client.post('/test-endpoint/', json=expected)
    assert response.status_code == 400
    assert response.json()['errors'][0]['title'] == "You provided 'my_prop'. It cannot be directly set via POST"

    response = json_client.put('/test-endpoint/0/', json=expected)
    assert response.status_code == 204

    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [expected]

    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == expected

def test_methods_post_not_method_property(app, json_client):
    signature = sepiida.fields.Object(s={
        'my_prop'   : sepiida.fields.String(methods=['GET', 'PUT']),
    })
    expected = {'my_prop': 'foo'}
    tests.helpers.create_expected_payload_endpoint(app, signature, {})

    response = json_client.post('/test-endpoint/', json=expected)
    assert response.status_code == 400
    assert response.json()['errors'][0]['title'] == "You provided 'my_prop'. It cannot be directly set via POST"

    response = json_client.post('/test-endpoint/', json={})
    assert response.status_code == 204

def test_methods_post_default(app, json_client):
    signature = sepiida.fields.Object(s={
        'prop1'   : sepiida.fields.String(default='foo'),
        'prop2'   : sepiida.fields.String(),
    })
    expected = {
        'prop1': 'foo',
        'prop2': 'a',
    }
    payload = {
        'prop2': 'a',
     }
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)

    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 204


@pytest.mark.parametrize('field, payload', [
    (sepiida.fields.String(choices=['foo', 'bar']),             {'my_prop': 'foo'}),
    (sepiida.fields.String(choices=['foo', 'bar']),             {'my_prop': 'bar'}),
    (sepiida.fields.String(choices=['foo', 'bar']),             {'my_prop': 'bar'}),
    (sepiida.fields.Integer(choices=[1, 2]),                    {'my_prop': 1}),
    (sepiida.fields.Integer(choices=[1, 2]),                    {'my_prop': 2}),
    (sepiida.fields.Float(choices=[1.1, 2.2]),                  {'my_prop': 1.1}),
    (sepiida.fields.Float(choices=[1.1, 2.2]),                  {'my_prop': 2.2}),
    (sepiida.fields.Boolean(choices=[True]),                    {'my_prop': True}),
    (sepiida.fields.Array(choices=[[1,'bar'], ['foo',4]]),   {'my_prop': [1, 'bar']}),
    (sepiida.fields.Array(choices=[[1,'bar'], ['foo',4]]),   {'my_prop': ['foo', 4]}),
    (sepiida.fields.Object(choices=[{'a': 1}, {'a': 'b'}]), {'my_prop': {'a': 1}}),
    (sepiida.fields.Object(choices=[{'a': 1}, {'a': 'b'}]), {'my_prop': {'a': 'b'}}),
])
def test_choices_happy_path(app, field, json_client, payload):
    signature = sepiida.fields.Object(s={
        'my_prop': field,
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, payload)

    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 204

    response = json_client.put('/test-endpoint/0/', json=payload)
    assert response.status_code == 204

@pytest.mark.parametrize('field, choices, selection', [
    (sepiida.fields.String,     ['foo', 'bar'],         'bad'),
    (sepiida.fields.Integer,    [1, 2],                 3),
    (sepiida.fields.Float,      [1.1, 2.2],             3.3),
    (sepiida.fields.Boolean,    [True],                 False),
    (sepiida.fields.Array,   [[1,'bar'], ['foo',4]], [4]),
    (sepiida.fields.Object, [{'a': 1}, {'a': 'b'}], {'c': 'd'}),
])
def test_choices_bad_choice(app, choices, field, json_client, selection):
    signature = sepiida.fields.Object(s={
        'my_prop': field(choices=choices),
    })
    payload = {
        'my_prop'   : selection,
    }
    tests.helpers.create_expected_payload_endpoint(app, signature, payload)

    _choices_error = ["'{}'".format(c) for c in choices]
    _choices_error = ", ".join(_choices_error)

    expected = {
        'errors'    : [{
            'code'  : 'property-value-not-in-allowed-values',
            'title' : "The property 'my_prop' must be one of {}. You provided '{}'".format(_choices_error, selection)
        }]
    }
    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 400
    assert response.json() == expected

    response = json_client.put('/test-endpoint/0/', json=payload)
    assert response.status_code == 400
    assert response.json() == expected


@pytest.mark.parametrize('field', [
    (sepiida.fields.String(optional=True)),
    (sepiida.fields.Integer(optional=True)),
    (sepiida.fields.Float(optional=True)),
    (sepiida.fields.Boolean(optional=True)),
    (sepiida.fields.Array(optional=True)),
    (sepiida.fields.Object(optional=True)),
    (sepiida.fields.Datetime(optional=True)),
    (sepiida.fields.Date(optional=True)),
    (sepiida.fields.UUID(optional=True)),
    (sepiida.fields.URI('', optional=True)),
    (sepiida.fields.URL(optional=True)),
])
def test_optional_response(app, field, json_client):
    signature = sepiida.fields.Object(s={
        'foo'   : field,
        'bar'   : sepiida.fields.String(),
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, {'bar': 'baz'})
    response = json_client.get('/test-endpoint/0/')

    assert response.status_code == 200
    assert response.json() == {'bar': 'baz'}

@pytest.mark.parametrize('field', [
    (sepiida.fields.String()),
    (sepiida.fields.Integer()),
    (sepiida.fields.Float()),
    (sepiida.fields.Boolean()),
    (sepiida.fields.Array()),
    (sepiida.fields.Object()),
    (sepiida.fields.Datetime()),
    (sepiida.fields.Date()),
    (sepiida.fields.UUID()),
    (sepiida.fields.URI('')),
    (sepiida.fields.URL()),
])
def test_optional_response_error(app, field, json_client):
    signature = sepiida.fields.Object(s={
        'foo'   : field,
        'bar'   : sepiida.fields.String(),
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, {'bar': 'baz'})
    with pytest.raises(Exception) as e:
        json_client.get('/test-endpoint/0/')
    assert str(e.value) == 'Programmer failed to provide foo, which is a required value to return for this resource.'

    app.config['DEBUG']   = False
    app.config['TESTING'] = False
    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 500
    app.config['DEBUG']   = True
    app.config['TESTING'] = True

def test_no_signature_json_object(app, json_client):
    signature = sepiida.fields.Object(s={
        'foo'   : sepiida.fields.Object(),
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, {'foo': {'bar': 'baz'}})
    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == {'foo': {'bar': 'baz'}}

@pytest.mark.parametrize('to_return, status_code, body, location', [
    ((None, 204, {'Location': 'here'}),                             204, '', 'http://sepiida.service/here'),
    (('',   204, {'Location': 'here'}),                             204, '', 'http://sepiida.service/here'),
    (lambda: flask.make_response('', 204, {'Location': 'here'}),    204, '', 'http://sepiida.service/here'),

    ((None, 204, {'Location': 'http://x.io/here'}),                             204, '', 'http://x.io/here'),
    (('',   204, {'Location': 'http://x.io/here'}),                             204, '', 'http://x.io/here'),
    (lambda: flask.make_response('', 204, {'Location': 'http://x.io/here'}),    204, '', 'http://x.io/here'),
])
def test_post_location_header(app, body, json_client, location, status_code, to_return):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={
            'foo'   : sepiida.fields.String(),
        })
        @staticmethod
        def post(uuid=None, _id=None, payload=None): # pylint: disable=unused-argument
            try:
                return to_return()
            except TypeError:
                return to_return

    TestEndpoint.add_resource(app)
    payload = {'foo': 'bar'}
    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == status_code
    assert response.data.decode('utf-8') == body
    assert response.headers['Location'] == location

def test_optional_subobject(app, json_client):
    signature = sepiida.fields.Object(s={
        'baz'       : sepiida.fields.String(),
        'foo'       : sepiida.fields.Object(s={
            'bar'   : sepiida.fields.String()
        }),
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, {'baz': 'test', 'foo': None})

    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == {'baz': 'test', 'foo': None}

@pytest.mark.parametrize('payload', [
    {'baz': ''},
    {'baz': '', 'foo': {}},
    {'baz': '', 'foo': {'bar': ''}},
    {'baz': '', 'foo': {'bif': ''}},
])
def test_default_subobject_all_default(app, json_client, payload):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={
            'baz'       : sepiida.fields.String(),
            'foo'       : sepiida.fields.Object(s={
                'bar'   : sepiida.fields.String(default='xyz'),
                'bif'   : sepiida.fields.String(default='pdq'),
            }, default={'bar': None, 'bif': None}),
        })
        @staticmethod
        def post(payload):
            assert 'foo' in payload
            assert 'bar' in payload['foo']
            assert 'bif' in payload['foo']
            return None

    TestEndpoint.add_resource(app)
    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 204

@pytest.mark.parametrize('field', [
    sepiida.fields.String,
    sepiida.fields.Integer,
    sepiida.fields.Float,
    sepiida.fields.Boolean,
    sepiida.fields.Object,
    sepiida.fields.Array,
    sepiida.fields.Datetime,
    sepiida.fields.Date,
    sepiida.fields.UUID,
    sepiida.fields.URI,
    sepiida.fields.URL,
])
def test_bad_signature_specifier_class(app, field, json_client): # pylint: disable=unused-argument
    with pytest.raises(Exception) as e:
        class TestEndpoint(sepiida.endpoints.APIEndpoint): # pylint: disable=W0612
            ENDPOINT = '/test-endpoint/'
            SIGNATURE = sepiida.fields.Object(s={
                'baz'       : field
            })
    assert str(e.value) == ("You have provided a class '{}' as part of the signature for baz."
        "Please specify the signature using instances of the class, not the class itself".format(field))

def test_field_rename(app, json_client):
    signature = sepiida.fields.Object(s={
        'foo'   : sepiida.fields.String(rename='bar'),
    })
    expected = {'bar': 'baz'}
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)
    response = json_client.get('/test-endpoint/{}/'.format(uuid4()))
    assert response.status_code == 200
    assert response.json() == {'foo': 'baz'}
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [{
        'foo': 'baz'
    }]
    response = json_client.post('/test-endpoint/', json={'foo': 'baz'})
    assert response.status_code == 204
    response = json_client.put('/test-endpoint/{}/'.format(uuid4()), json={'foo': 'baz'})
    assert response.status_code == 204

def test_field_rename_subsignature(app, json_client):
    signature = sepiida.fields.Object(s={
        'foo'       : sepiida.fields.Object(s={
            'bar'   : sepiida.fields.String(rename='baz'),
        })
    })
    expected = {'foo': {'baz': 'biff'}}
    tests.helpers.create_expected_payload_endpoint(app, signature, expected)
    response = json_client.get('/test-endpoint/{}/'.format(uuid4()))
    assert response.status_code == 200
    assert response.json() == {'foo': {'bar': 'biff'}}
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [{
        'foo': {'bar': 'biff'}
    }]
    response = json_client.post('/test-endpoint/', json={'foo': {'bar': 'biff'}})
    assert response.status_code == 204
    response = json_client.put('/test-endpoint/{}/'.format(uuid4()), json={'foo': {'bar': 'biff'}})
    assert response.status_code == 204

def test_field_rename_conflict():
    with pytest.raises(Exception) as e:
        sepiida.fields.Object(s={
            'foo'   : sepiida.fields.String(),
            'bar'   : sepiida.fields.String(rename='foo'),
        })
    assert str(e.value) == "You cannot rename field bar to foo because another field is already using that name"

@pytest.mark.parametrize('field', [
    sepiida.fields.String(filterable=False),
    sepiida.fields.Integer(filterable=False),
    sepiida.fields.Float(filterable=False),
    sepiida.fields.Boolean(filterable=False),
    sepiida.fields.Datetime(filterable=False),
    sepiida.fields.Date(filterable=False),
    sepiida.fields.UUID(filterable=False),
    sepiida.fields.URI('', filterable=False),
    sepiida.fields.URL(filterable=False),
])
def test_non_filterable_field(app, field, json_client):
    signature = sepiida.fields.Object(s={
        'bar'   : sepiida.fields.String(),
        'foo'   : field,
    })
    _create_unreachable_endpoint(app, signature)

    response = json_client.get('/test-endpoint/?filter[foo]=bar')
    assert response.status_code == 400
    expected = {
        'errors'    : [{
            'code'  : 'invalid-filter-property',
            'title' : ("Your requested filter property, 'foo', is not a valid property for this endpoint. "
                       "Please choose one of 'bar'"),
        }]
    }
    assert response.json() == expected

@pytest.mark.parametrize('field', [
    sepiida.fields.String(sortable=False),
    sepiida.fields.Integer(sortable=False),
    sepiida.fields.Float(sortable=False),
    sepiida.fields.Boolean(sortable=False),
    sepiida.fields.Datetime(sortable=False),
    sepiida.fields.Date(sortable=False),
    sepiida.fields.UUID(sortable=False),
    sepiida.fields.URI('', sortable=False),
    sepiida.fields.URL(sortable=False),
])
def test_non_sortable_field(app, field, json_client):
    signature = sepiida.fields.Object(s={
        'bar'   : sepiida.fields.String(),
        'baz'   : sepiida.fields.String(),
        'foo'   : field,
    })
    _create_unreachable_endpoint(app, signature)

    response = json_client.get('/test-endpoint/?sort=foo')
    assert response.status_code == 400
    expected = {
        'errors'    : [{
            'code'  : 'disabled-sort-property',
            'title' : ("Your requested sort property, 'foo', has sorting disabled for this endpoint. "
                       "Please remove it and try again. "
                       "The following fields are allowed for sorting: 'bar', 'baz'"),
        }]
    }
    assert response.json() == expected

def test_sorts_invalid_field(app, json_client):
    signature = sepiida.fields.Object(s={
        'bar'   : sepiida.fields.String(),
    })
    _create_unreachable_endpoint(app, signature)

    response = json_client.get('/test-endpoint/?sort=foo')
    assert response.status_code == 400
    expected = {
        'errors'    : [{
            'code'  : 'invalid-sort-property',
            'title' : ("Your requested sort property, 'foo', is not a valid property for this endpoint. "
                       "Please choose one of 'bar'"),
        }]
    }
    assert response.json() == expected

def test_sorts_extra_minus_sign(app, json_client):
    signature = sepiida.fields.Object(s={
        'bar'   : sepiida.fields.String(),
    })
    _create_unreachable_endpoint(app, signature)

    response = json_client.get('/test-endpoint/?sort=--bar')
    assert response.status_code == 400
    expected = {
        'errors'    : [{
            'code'  : 'invalid-sort-property',
            'title' : ("Your requested sort property, '-bar', is not a valid property for this endpoint. "
                       "Please choose one of 'bar'"),
        }]
    }
    assert response.json() == expected

@pytest.mark.parametrize('field, _filter', [
    (sepiida.fields.Integer(),              'bar'),
    (sepiida.fields.Integer(),              True),
    (sepiida.fields.Integer(),              4.2),
    (sepiida.fields.Float(),                'bar'),
    (sepiida.fields.Float(),                True),
    (sepiida.fields.Boolean(),              'fse'),
    (sepiida.fields.Boolean(),              'tru'),
    (sepiida.fields.Datetime(),             'a thing'),
    (sepiida.fields.Datetime(),             True),
    (sepiida.fields.Date(),                 'a thing'),
    (sepiida.fields.Date(),                 True),
    (sepiida.fields.UUID(),                 'a uuid'),
    (sepiida.fields.UUID(),                 True),
    (sepiida.fields.UUID(),                 4.2),
    (sepiida.fields.URI('test-endpoint'),   'a uri'),
    (sepiida.fields.URI('test-endpoint'),   True),
    (sepiida.fields.URI('test-endpoint'),   4.2),
    (sepiida.fields.URL(),                  'a url'),
    (sepiida.fields.URL(),                  '4.2'),
    (sepiida.fields.URL(),                  'true'),
])
def test_incorrect_filter_type(app, field, _filter, json_client):
    signature = sepiida.fields.Object(s={
        'foo'   : field,
    })
    _create_unreachable_endpoint(app, signature)
    url = '/test-endpoint/?filter[foo]={}'.format(_filter)
    response = json_client.get(url)
    assert response.status_code == 400
    expected = {
        'errors'    : [{
            'code'  : 'invalid-filter-value',
            'title' : ("You requested a filter on 'foo' to the value '{value}'. "
                       "The property 'foo' is a {type} and so filters on it must "
                       "also be {type}").format(value=_filter, type=field.TYPE_NAME),
        }]
    }
    assert response.json() == expected

FAKE_UUID = '01234567-0123-0123-0123-012345543210'
@pytest.mark.parametrize('field, _filter, expected', [
    (sepiida.fields.Integer(),  '2',                        [2]),
    (sepiida.fields.Integer(),  '2,3',                      [2, 3]),
    (sepiida.fields.Float(),    '1,0.1',                    [1, 0.1]),
    (sepiida.fields.Boolean(),  'True',                     [True]),
    (sepiida.fields.Boolean(),  'False',                    [False]),
    (sepiida.fields.Boolean(),  'true,f',                   [True, False]),
    (sepiida.fields.Boolean(),  'F,1',                      [False, True]),
    (sepiida.fields.Boolean(),  '0',                        [False]),
    (sepiida.fields.Datetime(), '2015-01-02T03:04:05.6',    [datetime(2015, 1, 2, 3, 4, 5, 600000, tzinfo=tzutc())]),
    (sepiida.fields.Datetime(), '2015-01-02T03:04:05',      [datetime(2015, 1, 2, 3, 4, 5, 0, tzinfo=tzutc())]),
    (sepiida.fields.Date(),     '2015-01-02',               [date(2015, 1, 2)]),
    (sepiida.fields.UUID(),     FAKE_UUID,                  [UUID(FAKE_UUID)]),
    (sepiida.fields.URL(),      'https://somewhere/',       ['https://somewhere/']),
])
def test_filter_conversions(app, expected, field, _filter, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={'foo': field})
        def list(self):
            for key, filters in self.filters.items():
                assert key == 'foo'
                for _filter in filters:
                    assert _filter.values == expected

    TestEndpoint.add_resource(app)
    url = '/test-endpoint/?filter[foo]={}'.format(_filter)
    response = json_client.get(url)
    assert response.status_code == 204, response.json()

@pytest.mark.parametrize('expected, resource_params', [
    (123,               {'_id': 123}),
    (UUID(FAKE_UUID),   {'uuid': FAKE_UUID}),
])
def test_filter_conversion_uri(app, expected, json_client, resource_params):
    class SomeResource(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/a-resource/'
        SIGNATURE = sepiida.fields.Object()
        @staticmethod
        def get(_id=None, uuid=None): # pylint: disable=unused-argument
            return {}

    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={
            'foo': sepiida.fields.URI('some-resource')
        })
        def list(self):
            for key, filters in self.filters.items():
                assert key == 'foo'
                for _filter in filters:
                    assert _filter.values == [expected]

    TestEndpoint.add_resource(app)
    SomeResource.add_resource(app, 'some-resource')

    resource_uri = flask.url_for('some-resource', _method='GET', _external=True, **resource_params)
    url = '/test-endpoint/?filter[foo]={}'.format(resource_uri)
    response = json_client.get(url)
    assert response.status_code == 204, response.json()

def test_filter_choices(app, json_client):
    signature = sepiida.fields.Object(s={
        'foo'   : sepiida.fields.String(choices=['bar', 'baz']),
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, {'foo': 'bar'})
    response = json_client.get('/test-endpoint/?filter[foo]=invalid')
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'invalid-filter-value',
        'title' : ("You requested a filter on 'foo' to the value 'invalid'. "
                   "The property 'foo' only permits values from a particular "
                   "set of options. Please use one of the following options "
                   "for your filter: bar, baz")
        }]
    }
    assert response.json() == expected

@pytest.mark.parametrize('field', [
    sepiida.fields.String(),
    sepiida.fields.Integer(),
    sepiida.fields.Float(),
    sepiida.fields.Boolean(),
    sepiida.fields.Datetime(),
    sepiida.fields.Date(),
    sepiida.fields.UUID(),
    sepiida.fields.URI(''),
    sepiida.fields.URL(),
    sepiida.fields.Object(),
])
def test_options_gives_fields_information(app, field, json_client):
    signature = sepiida.fields.Object(s={
        'test-field'    : field,
    })
    _create_unreachable_endpoint(app, signature)

    response = json_client.options('/test-endpoint/')
    assert response.status_code == 200
    expected = {
        'signature'             : {
            'choices'           : None,
            'fields'            : {
                'test-field'    : field.get_options_description(),
            },
            'filterable'        : True,
            'methods'           : ['GET', 'POST', 'PUT'],
            'optional'          : False,
            'sortable'          : True,
            'type'              : 'object',
            'searchable'        : True,
        }
    }
    assert response.json() == expected

    # make sure we can deal with ID and UUID
    response = json_client.options('/test-endpoint/1/')
    assert response.status_code == 200

    uuid = uuid4()
    response = json_client.options('/test-endpoint/{}/'.format(uuid))
    assert response.status_code == 200

def _setup_uri_field_get(app, payload):
    signature = sepiida.fields.Object(s={
        'some_uri'  : sepiida.fields.URI(endpoint='other-endpoint'),
    })
    tests.helpers.create_expected_payload_endpoint(app, signature, payload)

    @app.route('/somewhere/<uuid:frobble>/', endpoint='other-endpoint')
    def somewhere(): # pylint: disable=unused-variable
        return '', 204

def test_uri_field_list_internal(app, json_client):
    uuid = uuid4()
    payload = {'some_uri': {'frobble': uuid}}
    _setup_uri_field_get(app, payload)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    expected = [{
        'some_uri'  : sepiida.routing.uri('other-endpoint', frobble=uuid),
    }]
    assert response.json()['resources'] == expected

def test_uri_field_list_external(app, json_client):
    payload = {'some_uri': 'https://sepiida.service/somewhere/{}/'.format(uuid4())}
    _setup_uri_field_get(app, payload)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    expected = [{
        'some_uri'  : payload['some_uri'],
    }]
    assert response.json()['resources'] == expected

def test_uri_field_filter_external_service(app, json_client):
    """
    Test error messages related to bad URI field. We want the URI field to be for
    a non-internal-service URI and ensure the error message makes sense
    """
    signature = sepiida.fields.Object(s={
        'some_uri'  : sepiida.fields.URI(endpoint='non-existent-endpoint'),
    })
    payload = {'some_uri': 'https://users.service/user/{}/'.format(uuid4())}
    tests.helpers.create_expected_payload_endpoint(app, signature, payload)
    filter_uri = 'https://users.service/user/{}/'.format(uuid4())
    response = json_client.get('/test-endpoint/?filter[some_uri]={}'.format(filter_uri))
    assert response.status_code == 500
    expected = {'errors': [{
        'code'  : 'app-error',
        'title' : "The endpoint 'non-existent-endpoint' is not a valid endpoint for this application",
    }]}
    assert response.json() == expected

def _setup_uri_field_post(app, check_payload):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={
            'some_uri'  : sepiida.fields.URI(endpoint='other-endpoint'),
        })
        @staticmethod
        def post(payload):
            check_payload(payload['some_uri'])
        @staticmethod
        def list():
            return []

    @app.route('/somewhere/<uuid:frobble>/', endpoint='other-endpoint')
    def somewhere(): # pylint: disable=unused-variable
        return '', 204

    TestEndpoint.add_resource(app, 'test_endpoint')

def test_uri_field_post_recognized(app, json_client):
    uuid = uuid4()
    def _check_payload(payload):
        assert payload == {
            'uri'       : flask.url_for('other-endpoint', frobble=uuid, _external=True),
            'frobble'   : uuid,
        }
    _setup_uri_field_post(app, _check_payload)
    payload = {
        'some_uri'  : flask.url_for('other-endpoint', frobble=uuid, _external=True),
    }
    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 204

@pytest.mark.parametrize('uri', [
    'https://www.google.com',
    'www.google.com',
    'I-am-dumb',
])
def test_uri_field_post_unrecognized(app, json_client, uri):
    _setup_uri_field_post(app, lambda x: x)
    payload = {'some_uri': uri}
    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 400
    assert response.json() == {'errors': [{
        'code'  : 'request-validation-error',
        'title' : 'The URI you provided for some_uri - {} - is not recognized as a URI'.format(uri),
    }]}

def test_uri_field_wrong_uri(app, json_client):
    _setup_uri_field_post(app, lambda x: x)
    other_uri = 'http://localhost/test-endpoint/'
    payload = {'some_uri': other_uri}
    response = json_client.post('/test-endpoint/', json=payload)
    assert response.status_code == 400
    expected = {'errors': [{
        'code'  : 'request-validation-error',
        'title' : ('The URI you provided for some_uri - {} - appears to be a URI for a test_endpoint resource, '
                    'not a other-endpoint resource'.format(other_uri)),
    }]}
    assert response.json() == expected

@pytest.mark.parametrize('payload_creator', (
    'https://sepiida.service/test-endpoint/{}/'.format,
    lambda u: {'uuid': u},
    lambda u: u,
))
def test_uri_field_package(app, json_client, payload_creator):
    "Test that the client can return various payload types and the URI field will convert it appropriately"
    _uuid = uuid4()
    signature = sepiida.fields.Object(s={
        'my_prop'   : sepiida.fields.URI('test_endpoint'),
    })
    payload = {
        'my_prop'   : payload_creator(_uuid),
    }
    tests.helpers.create_expected_payload_endpoint(app, signature, payload)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    expected = [{
        'my_prop'   : sepiida.routing.uri('test_endpoint', _uuid),
    }]
    assert response.json()['resources'] == expected

def _setup_url_test_endpoint(app, response='http://google.com'):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={
            'some_url'  : sepiida.fields.URL(),
        })
        @staticmethod
        def post(payload):
            return payload

        @staticmethod
        def list():
            return [{'some_url': response}]
    TestEndpoint.add_resource(app)

@pytest.mark.parametrize('inp, out', (
    ({'some_url': 'https://somewhere.com'},                 {'some_url': 'https://somewhere.com'}),
    ({'some_url': 'https://somewhere.com/foo'},             {'some_url': 'https://somewhere.com/foo'}),
    ({'some_url': 'https://somewhere.com/foo?bar'},         {'some_url': 'https://somewhere.com/foo?bar'}),
    ({'some_url': 'https://somewhere.com/foo?bar#baz'},     {'some_url': 'https://somewhere.com/foo?bar#baz'}),
    ({'some_url': 'https://user:password@somewhere.com'},   {'some_url': 'https://somewhere.com'}),
    ({'some_url': 'https://user@somewhere.com'},            {'some_url': 'https://somewhere.com'}),
    ({'some_url': 'https://:password@somewhere.com'},       {'some_url': 'https://somewhere.com'}),
))
def test_url_field_post(app, inp, json_client, out):
    _setup_url_test_endpoint(app)
    response = json_client.post('/test-endpoint/', json=inp)
    assert response.status_code == 201
    assert response.json() == out

@pytest.mark.parametrize('inp, out', (
    ('https://somewhere.com',                 {'some_url': 'https://somewhere.com'}),
    ('https://somewhere.com/foo',             {'some_url': 'https://somewhere.com/foo'}),
    ('https://somewhere.com/foo?bar',         {'some_url': 'https://somewhere.com/foo?bar'}),
    ('https://somewhere.com/foo?bar#baz',     {'some_url': 'https://somewhere.com/foo?bar#baz'}),
    ('https://user:password@somewhere.com',   {'some_url': 'https://somewhere.com'}),
    ('https://user@somewhere.com',            {'some_url': 'https://somewhere.com'}),
    ('https://:password@somewhere.com',       {'some_url': 'https://somewhere.com'}),
))
def test_url_field_get(inp, app, json_client, out):
    _setup_url_test_endpoint(app, inp)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json()['resources'] == [out]

@pytest.mark.parametrize('field, typename', (
    [sepiida.fields.Object(), 'object'],
    [sepiida.fields.Anything(), 'anything'],
))
def test_options_on_empty_json_signature(app, field, json_client, typename):
    "Ensure we can still do OPTIONS on Object with an emtpy signature. AE-1526"
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = field
        @staticmethod
        def post(payload):
            return payload
    TestEndpoint.add_resource(app)
    response = json_client.options('/test-endpoint/')
    assert response.status_code == 200
    assert response.json() == {
        'signature'         : {
            'choices'       : None,
            'filterable'    : True,
            'methods'       : ['GET', 'POST', 'PUT'],
            'optional'      : False,
            'sortable'      : True,
            'type'          : typename,
            'searchable'    : True,
        }
    }

@pytest.mark.parametrize('content', [
    [{'foo': 1, 'bar': 2, 'baz': 3}],
    [1, 2, 3],
    {'foo': 1, 'bar': 2, 'baz': 3},
    1,
    None,
    'flubber',
    3.14,
])
def test_anything_field(app, content, json_client):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Object(s={
            'content'   : sepiida.fields.Anything(),
        })
        @staticmethod
        def get(_id): # pylint: disable=unused-argument
            return {'content': content}
    TestEndpoint.add_resource(app)

    response = json_client.get('/test-endpoint/0/')
    assert response.status_code == 200
    assert response.json() == {'content': content}
