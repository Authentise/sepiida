import json
import urllib.parse
import uuid

import sepiida.responses
import sepiida.session


def _error_response(response, code, title):
    expected = {
        'errors'    : [{
            'code'  : code,
            'title' : title,
        }]
    }
    return response.json() == expected

def test_session_required_none_provided(app, json_client):
    @app.route('/test-session-required-bad', methods=['POST'])
    def _handle_post(): # pylint: disable=unused-variable
        assert False, "We should never reach this code"
    sepiida.session.register_session_handlers(app)
    response = json_client.post('/test-session-required-bad', json={'test': 'a'})
    assert response.status_code == 403
    assert response.headers['WWW-Authenticate'] == 'Custom realm="Authentise"'
    assert _error_response(response, code='no-credentials-provided', title=(
        'You must include a valid session cookie or an API '
        'token and secret encoded in your Authorization header according '
        'to Basic Auth standards (RFC 2617)'))

def test_session_required_bad_session_cookie(app, json_client):
    @app.route('/test-session-required-bad', methods=['POST'])
    def _handle_post(): # pylint: disable=unused-variable
        assert False, "We should never reach this code"
    sepiida.session.register_session_handlers(app)
    response = json_client.post('/test-session-required-bad', json={'test': 'a'}, headers={'cookie': 'session=some bad session cookie'})
    assert response.status_code == 403
    assert response.headers['WWW-Authenticate'] == 'Custom realm="Authentise"'
    assert _error_response(response, code='empty-session-provided', title='The session cookie you have provided does not contain any data')

def test_session_required_malformed_auth(app, json_client):
    @app.route('/test-session-required-bad', methods=['POST'])
    def _handle_post(): # pylint: disable=unused-variable
        assert False, "We should never reach this code"
    sepiida.session.register_session_handlers(app)
    response = json_client.post('/test-session-required-bad', json={'test': 'a'}, headers={'Authorization': 'bad auth'})
    assert response.status_code == 400
    assert response.headers['WWW-Authenticate'] == 'Custom realm="Authentise"'
    assert _error_response(
            response,
            code='not-well-formed-authorization-header',
            title='The authorization header you have provided was not properly formatted. Please see RFC 2617')

def test_session_required_invalid_auth(app, httpretty, json_client, settings):
    @app.route('/test-session-required-bad', methods=['POST'])
    def _handle_post(): # pylint: disable=unused-variable
        assert False, "We should never reach this code"
    sepiida.session.register_session_handlers(app)

    httpretty.register_uri(
        httpretty.GET,
        urllib.parse.urljoin(settings.USER_SERVICE, '/sessions/'),
        status=404,
    )
    response = json_client.post('/test-session-required-bad', json={'test': 'a'}, auth=('invalid', 'invalid'))
    assert response.status_code == 403
    assert response.headers['WWW-Authenticate'] == 'Custom realm="Authentise"'
    assert _error_response(response, code='invalid-credentials', title='The API key and secret you provided were not recognized')

def _add_session_required_post(app, expected_uri):
    @app.route('/test-session-required-good', methods=['POST'])
    def _handle_post(): # pylint: disable=unused-variable
        assert sepiida.session.current_user_uri() == expected_uri
        return '', 201
    sepiida.session.register_session_handlers(app)

def test_session_required_valid_auth(app, basic_auth, httpretty, json_client, pao_user, settings):
    _add_session_required_post(app, pao_user['uri'])
    httpretty.register_uri(
        httpretty.GET,
        urllib.parse.urljoin(settings.USER_SERVICE, '/sessions/'),
        body=json.dumps({'uri': pao_user['uri']}),
        status=200,
    )
    response = json_client.post('/test-session-required-good',
        json={'test': 'a'},
        auth=(pao_user['username'], pao_user['password'])
    )
    assert response.status_code == 201

    my_credentials = basic_auth(pao_user['username'], pao_user['password'])
    assert httpretty.httpretty.latest_requests[0].headers['Authorization'] == my_credentials

def test_session_required_good_session_cookie(app, json_client, user):
    _add_session_required_post(app, user['uri'])
    with json_client.session_transaction() as my_session:
        my_session['user_uri'] = user['uri']
    response = json_client.post('/test-session-required-good', json={'test': 'a'})
    assert response.status_code == 201

def test_session_required_good_session_cookie_via_fixture(app, json_client_session, user):
    _add_session_required_post(app, user['uri'])
    response = json_client_session.post('/test-session-required-good', json={'test': 'a'})
    assert response.status_code == 201

def test_session_whitelist_no_decorator(app, json_client):
    @app.route('/test-session-not-required', methods=['POST'])
    def _test_session_not_required1(): # pylint: disable=unused-variable
        assert sepiida.session.current_user_uri() is None
        return '', 201

    sepiida.session.register_session_handlers(app, whitelist=['_test_session_not_required1'])
    response = json_client.post('/test-session-not-required', json={'test': 'a'})
    assert response.status_code == 201

def test_not_found_route(app, json_client):
    sepiida.session.register_session_handlers(app)
    response = json_client.get('/not-found')
    assert response.status_code == 404

def test_not_found_route_valid_resource(app, json_client, user):
    @app.route('/resource-invalid-method/', methods=['POST'])
    def _test_post_resource_invalid_method(): # pylint: disable=unused-variable
        return '', 201
    sepiida.session.register_session_handlers(app)
    with json_client.session_transaction() as session:
        session['user_uri'] = user['uri']
    response = json_client.post('/resource-invalid-method/', json={})
    assert response.status_code == 201
    response = json_client.get('/resource-invalid-method/')
    assert response.status_code == 405

def test_current_user(app, json_client, user):
    session_uuid = str(uuid.uuid4())
    @app.route('/test-request', methods=['GET'])
    def _test_request(): # pylint: disable=unused-variable
        expected = {
            'emails'        : user['emails'],
            'impersonator'  : user['uri'],
            'name'          : user['name'],
            'uri'           : user['uri'],
            'username'      : user['username'],
            'uuid'          : session_uuid,
        }
        assert sepiida.session.current_user() == expected
        return '', 201
    sepiida.session.register_session_handlers(app)

    with json_client.session_transaction() as session:
        session['emails']       = user['emails']
        session['impersonator'] = user['uri']
        session['name']         = user['name']
        session['uri']          = user['uri']
        session['username']     = user['username']
        session['user_uri']     = user['uri']
        session['uuid']         = session_uuid
    json_client.get('/test-request')

def _add_test_is_app_user_route(app):
    @app.route('/test-is-api-user/', methods=['GET'])
    def _test_is_app_user(): # pylint: disable=unused-variable
        return sepiida.responses.json_response({
            'is_api_user'       : sepiida.session.is_api_user(),
            'is_internal_user'  : sepiida.session.is_internal_user(),
        })
    sepiida.session.register_session_handlers(app)


def test_is_api_user_no_session(app, httpretty, json_client, settings):
    _add_test_is_app_user_route(app)
    # No session at all
    httpretty.register_uri(httpretty.GET,
        urllib.parse.urljoin(settings.USER_SERVICE, '/sessions/'),
        status=404
    )
    response = json_client.get('/test-is-api-user/')
    assert response.status_code == 403

def test_is_api_user_session(app, json_client, user):
    _add_test_is_app_user_route(app)
    with json_client.session_transaction() as session:
        session['user_uri'] = user['uri']
    response = json_client.get('/test-is-api-user/')
    assert response.status_code == 200
    assert response.json() == {
        'is_api_user'       : False,
        'is_internal_user'  : False,
    }

def test_is_api_user_reg_api_user(app, json_client, httpretty, settings, user):
    _add_test_is_app_user_route(app)
    # Regular session
    httpretty.register_uri(httpretty.GET,
        urllib.parse.urljoin(settings.USER_SERVICE, '/sessions/'),
        body=json.dumps(user)
    )
    response = json_client.get('/test-is-api-user/', auth=('some-user', 'some-password'))
    assert response.status_code == 200
    assert response.json() == {
        'is_api_user'       : True,
        'is_internal_user'  : False,
    }

def test_is_api_user_internal_api_user(app, json_client, settings):
    _add_test_is_app_user_route(app)
    response = json_client.get('/test-is-api-user/', auth=('api', settings.API_TOKEN))
    assert response.status_code == 200
    assert response.json() == {
        'is_api_user'       : True,
        'is_internal_user'  : True,
    }

def test_static_no_session(app, json_client, monkeypatch):
    monkeypatch.setattr(app, 'static_url_path', '/static')
    @app.route(app.static_url_path + '/test-static-session/')
    def _static_asset(): # pylint: disable=unused-variable
        return 'thing', 200

    sepiida.session.register_session_handlers(app)
    response = json_client.get(app.static_url_path + '/test-static-session/')
    assert response.status_code == 200

def _add_request_id_get(app):
    @app.route('/test-path/', methods=['GET'])
    def _handle_get(): # pylint: disable=unused-variable
        return sepiida.session.current_request_id(), 200

def test_generate_request_id_not_configured(app, json_client):
    "Ensure that if we don't configure the request ID handler that we still get a value for current_request_id()"
    _add_request_id_get(app)
    response = json_client.get('/test-path/')
    assert response.status_code == 200
    assert response.data == 'none'.encode('utf-8')

def test_generate_request_id(app, mocker, json_client):
    "Ensure that we properly generate a request ID when we don't already have one"
    _add_request_id_get(app)
    sepiida.session.add_request_id_handler(app)
    mocker.patch('uuid.uuid4', return_value='abc-123')
    response = json_client.get('/test-path/')
    assert response.status_code == 200
    assert response.data == 'abc-123'.encode('utf-8')
    assert response.headers['Request-ID']

def test_maintain_request_id(app, mocker, json_client):
    "Ensure we perpetuate the request ID if one is already present"
    _add_request_id_get(app)
    sepiida.session.add_request_id_handler(app)
    mocker.patch('uuid.uuid4', return_value='abc-123')
    response = json_client.get('/test-path/', headers={'Request-ID': 'xyz-pdq'})
    assert response.status_code == 200
    assert response.data == 'xyz-pdq'.encode('utf-8')
