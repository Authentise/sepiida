import uuid
from contextlib import contextmanager
from datetime import timedelta

import celery.result
import flask
import pytest

import sepiida.backend
import sepiida.context
import sepiida.endpoints


@pytest.mark.usefixtures('backend_app')
def test_backend_task_decorator_standard_call():
    task_ran = False
    @sepiida.backend.task()
    def task():
        nonlocal task_ran
        task_ran = True

    assert not task_ran

    task()
    assert task_ran

@pytest.mark.usefixtures('backend_app')
def test_backend_task_decorator_delayed():
    task_ran = False
    @sepiida.backend.task()
    def task():
        nonlocal task_ran
        task_ran = True

    assert not task_ran

    result = task.delay()
    assert isinstance(result, celery.result.EagerResult)
    assert task_ran

@pytest.mark.usefixtures('backend_app')
def test_backend_task_decorator_with_args():
    _sum = None
    @sepiida.backend.task()
    def task(arg0, arg1):
        nonlocal _sum
        _sum = arg0 + arg1

    assert not _sum

    task(2,3)
    assert _sum == 5

    result = task.delay(40,2)
    assert isinstance(result, celery.result.EagerResult)
    assert _sum == 42

def test_backend_task_decorator_args(backend_app):
    @sepiida.backend.task()
    def task0(): #pylint: disable=unused-variable
        pass

    @sepiida.backend.task(max_retries = 10, default_retry_delay=1)
    def task1(): #pylint: disable=unused-variable
        pass

    sepiida.backend.TaskProxy.register_all()

    assert backend_app.tasks['tests.test_backend.task0'].max_retries == 3
    assert backend_app.tasks['tests.test_backend.task0'].default_retry_delay == 60 * 3

    assert backend_app.tasks['tests.test_backend.task1'].max_retries == 10
    assert backend_app.tasks['tests.test_backend.task1'].default_retry_delay == 1

def test_backend_task_decorator_schedule(backend_app):
    schedule0 = timedelta(seconds=42)
    schedule1 = timedelta(seconds=4)

    @sepiida.backend.task(schedule=schedule0)
    def scheduled_task0(): #pylint: disable=unused-variable
        pass
    scheduled_task0()

    @sepiida.backend.task(schedule=schedule1)
    def scheduled_task1(): #pylint: disable=unused-variable
        pass
    scheduled_task1()

    @sepiida.backend.task(schedule=timedelta(seconds=2))
    def scheduled_task2(): #pylint: disable=unused-variable
        pass

    expected_name0 = 'tests.test_backend.scheduled_task0'
    expected_name1 = 'tests.test_backend.scheduled_task1'
    expected_name2 = 'tests.test_backend.scheduled_task2'
    expected = {
        expected_name0: {
            'task'      : expected_name0,
            'schedule'  : schedule0,
        },
        expected_name1: {
            'task'      : expected_name1,
            'schedule'  : schedule1,
        }
    }
    assert backend_app.conf["CELERYBEAT_SCHEDULE"] == expected
    assert str(backend_app.timezone) == "UTC"
    assert expected_name0 in backend_app.tasks
    assert expected_name1 in backend_app.tasks
    assert expected_name2 not in backend_app.tasks

@pytest.mark.usefixtures('backend_app')
def test_backend_task_retry():
    attempts = 0
    @sepiida.backend.task()
    def task():
        nonlocal attempts
        attempts += 1 #pylint: disable=unused-variable
        raise task.retry()

    result = task.delay()
    assert attempts == 4
    with pytest.raises(celery.exceptions.MaxRetriesExceededError):
        result.get()

@contextmanager
def _backend_app(flask_app=None):
    backend_app = sepiida.backend.create('test_backend', testing=True, flask_app=flask_app)
    yield backend_app
    backend_app.close()
    for instance in sepiida.backend.TaskProxy.instances:
        instance.celery_task = None
    sepiida.backend.create.application = None
    sepiida.backend.create.flask_app = None

def test_backend_task_no_flask_app_context():
    flask_app = flask.Flask('test')
    flask_app.config['SERVER_NAME'] = 'test.service'
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def get(_uuid):
            return _uuid
    TestEndpoint.add_resource(flask_app, 'some-resource')

    with _backend_app():
        task_ran = False
        @sepiida.backend.task()
        def task():
            nonlocal task_ran
            _uuid = uuid.uuid4()
            sepiida.routing.uri('some-resource', _uuid)
            task_ran = True

        result = task.delay()
    assert isinstance(result, celery.result.EagerResult)
    assert result.status == 'FAILURE'

def test_backend_task_flask_app_context():
    flask_app = flask.Flask('test')
    flask_app.config['SERVER_NAME'] = 'test.service'
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def get(_uuid):
            return _uuid
    TestEndpoint.add_resource(flask_app, 'some-resource')

    with _backend_app(flask_app):
        task_ran = False
        @sepiida.backend.task(bind=True)
        @sepiida.backend.with_app_context
        def task(self): #pylint: disable=unused-argument
            nonlocal task_ran
            _uuid = uuid.uuid4()
            sepiida.routing.uri('some-resource', _uuid)
            task_ran = True

        result = task.delay()
    assert isinstance(result, celery.result.EagerResult)
    assert task_ran

@pytest.mark.usefixtures('backend_app')
def test_backend_task_flask_app_context_fixtures(app):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def get(_uuid):
            return _uuid
    TestEndpoint.add_resource(app, 'some-resource')

    task_ran = False
    @sepiida.backend.task()
    def task():
        nonlocal task_ran
        _uuid = uuid.uuid4()
        sepiida.routing.uri('some-resource', _uuid)
        task_ran = True

    result = task.delay()
    assert isinstance(result, celery.result.EagerResult)
    assert task_ran

def test_backend_task_apply_async_with_app_context():
    flask_app = flask.Flask('test')
    flask_app.config['SERVER_NAME'] = 'test.service'
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def get(_uuid):
            return _uuid
    TestEndpoint.add_resource(flask_app, 'some-resource')

    with _backend_app(flask_app):
        task_ran = False
        @sepiida.backend.task(bind=True)
        @sepiida.backend.with_app_context
        def task(self): #pylint: disable=unused-argument
            nonlocal task_ran
            _uuid = uuid.uuid4()
            sepiida.routing.uri('some-resource', _uuid)
            task_ran = True

        result = task.apply_async()
    assert isinstance(result, celery.result.EagerResult)
    assert task_ran

def test_backend_task_flask_app_context_standard_call():
    flask_app = flask.Flask('test')
    flask_app.config['SERVER_NAME'] = 'test.service'
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def get(_uuid):
            return _uuid
    TestEndpoint.add_resource(flask_app, 'some-resource')

    with _backend_app(flask_app):
        task_ran = False
        @sepiida.backend.task(bind=True)
        @sepiida.backend.with_app_context
        def task(self): #pylint: disable=unused-argument
            nonlocal task_ran
            _uuid = uuid.uuid4()
            sepiida.routing.uri('some-resource', _uuid)
            task_ran = True

        task() #pylint: disable=no-value-for-parameter
    assert task_ran

@pytest.mark.usefixtures('backend_app')
def test_backend_task_flask_app_passes_credentials(app, json_client_session):
    "Ensure that tasks passed from frontend to backend include the user credentials data"
    @sepiida.backend.task(bind=True)
    def task(self): #pylint: disable=unused-argument
        return self.credentials

    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            results = task.delay().get() #pylint: disable=no-value-for-parameter
            assert results == sepiida.context.extract_credentials(flask.request)
            return []
    TestEndpoint.add_resource(app, 'some-resource')
    response = json_client_session.get('/test-endpoint/')
    assert response.status_code == 200
    assert response
