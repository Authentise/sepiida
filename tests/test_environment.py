import pytest

import sepiida.environment

import tests.resources.environment

def _create_spec():
    return sepiida.environment.Specification(
        foo = sepiida.environment.Variable(docs='The foo variable'),
        bar = sepiida.environment.Variable(docs='The bar variable')
    )

def test_environment_spec(fakeenv):
    fakeenv['foo'] = 'something'
    fakeenv['bar'] = 'another thing'
    spec = _create_spec()
    settings = spec.parse()
    assert settings['foo'] == 'something'
    assert settings.foo == 'something'
    assert settings['bar'] == 'another thing'
    assert settings.bar == 'another thing'

def test_environment_required():
    spec = _create_spec()
    with pytest.raises(sepiida.environment.ParseError) as e:
        spec.parse()
    assert e.value.errors == {
        sepiida.environment.UndefinedError('The variable bar was undefined and has no default value'),
        sepiida.environment.UndefinedError('The variable foo was undefined and has no default value'),
    }

@pytest.mark.parametrize('type_, value, expected', (
    [sepiida.environment.VariableBoolean, '0', False],
    [sepiida.environment.VariableBoolean, '1', True],
    [sepiida.environment.VariableBoolean, 'f', False],
    [sepiida.environment.VariableBoolean, 't', True],
    [sepiida.environment.VariableBoolean, 'F', False],
    [sepiida.environment.VariableBoolean, 'T', True],
    [sepiida.environment.VariableBoolean, 'false', False],
    [sepiida.environment.VariableBoolean, 'true', True],
    [sepiida.environment.VariableBoolean, 'False', False],
    [sepiida.environment.VariableBoolean, 'True', True],
    [sepiida.environment.VariableFloat, '3.14', 3.14],
    [sepiida.environment.VariableInteger, '3', 3],
    [sepiida.environment.VariableList, '1;2;3;a;b;c', ['1', '2', '3', 'a', 'b', 'c']],
))
def test_environment_parsing(expected, fakeenv, type_, value):
    fakeenv['somevar'] = value

    fakeenv['integer']  = '3'
    fakeenv['list']     = '1;2;3;a;b;c'
    spec = sepiida.environment.Specification(
        somevar = type_(),
    )
    settings = spec.parse()
    assert settings['somevar'] == expected

def test_environment_custom_parser(fakeenv):
    fakeenv['strange_boolean']  = 'fizz1buzz'
    fakeenv['strange_float']    = '.0'
    fakeenv['strange_integer']  = '3'
    fakeenv['strange_list']     = '3;'
    fakeenv['strange_string']   = 'wee'
    spec = sepiida.environment.Specification(
        strange_boolean = sepiida.environment.VariableBoolean(parser=lambda x: x[4:5]),
        strange_float   = sepiida.environment.VariableFloat(parser=lambda x: '33' + x),
        strange_integer = sepiida.environment.VariableInteger(parser=lambda x: 4 * x),
        strange_list    = sepiida.environment.VariableList(parser=lambda x: x*2),
        strange_string  = sepiida.environment.Variable(parser=lambda x: x.title()),
    )
    settings = spec.parse()
    assert settings['strange_boolean'] is True
    assert settings.strange_float == 33.0
    assert settings['strange_integer'] == 3333
    assert settings.strange_list == ('3', '3', '')
    assert settings['strange_string'] == 'Wee'

@pytest.mark.parametrize('variabletype, value', [
    (sepiida.environment.VariableInteger, 'a'),
    (sepiida.environment.VariableInteger, ''),
    (sepiida.environment.VariableInteger, '1.3'),
    (sepiida.environment.VariableInteger, '--3'),
    (sepiida.environment.VariableBoolean, ''),
    (sepiida.environment.VariableFloat,   'a'),
    (sepiida.environment.VariableFloat,   ''),
    (sepiida.environment.VariableFloat,   '_'),
    (sepiida.environment.VariableFloat,   '--3'),
])
def test_environment_parsing_failure(fakeenv, variabletype, value):
    spec = sepiida.environment.Specification(
        test = variabletype(),
    )
    fakeenv['test'] = value
    with pytest.raises(sepiida.environment.ParseError):
        spec.parse()

def test_get_without_parse():
    settings = sepiida.environment.parse.settings
    sepiida.environment.parse.settings = None
    with pytest.raises(Exception):
        sepiida.environment.get()
    sepiida.environment.parse.settings = settings

def test_bad_parse():
    spec = _create_spec()
    with pytest.raises(SystemExit) as e:
        sepiida.environment.parse(spec)
    assert "variable foo was undefined" in str(e.value)
    assert "variable bar was undefined" in str(e.value)

def test_defaults():
    spec = sepiida.environment.Specification(
        foo = sepiida.environment.Variable(default='thing1'),
        bar = sepiida.environment.Variable(default='thing2')
    )
    settings = spec.parse()
    assert settings.foo == 'thing1'
    assert settings.bar == 'thing2'

def test_builtins(fakeenv):
    "Test that clients can add to the built-in variables"
    oldsettings = sepiida.environment.parse.settings
    fakeenv['foo'] = 'something'
    fakeenv['bar'] = 'another thing'
    fakeenv['STORAGE_SERVICE'] = 'some.storage'
    spec = _create_spec()
    settings = sepiida.environment.parse(spec)
    assert settings.foo == 'something'
    assert settings['bar'] == 'another thing'
    assert settings.STORAGE_SERVICE == 'some.storage'
    sepiida.environment.parse.settings = oldsettings



@pytest.mark.skip("needs a specific environment setup")
def test_can_load_from_docker_secrets():
    # for this test to run, you need to have set/loaded EXAMPLE_SECRET_VIA_DOCKER as
    # docker secret, with the value 'Test Value. Hoagies Are Great' via
    # `printf "Test Value. Hoagies Are Great" | docker secret create EXAMPLE_SECRETS_VIA_DOCKER -`
    # or similar. Not available in all unittest situations
    settings = tests.resources.environment.parse()
    expected  = {'EXAMPLE_SECRET_VIA_DOCKER':'Test Value. Hoagies Are Great'}
    assert settings['EXAMPLE_SECRET_VIA_DOCKER'] ==  expected['EXAMPLE_SECRET_VIA_DOCKER']
