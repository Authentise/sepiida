import pytest
import werkzeug.wsgi

import sepiida.wsgi


def test_stabilize_werkzeug_iterator():
    class FakeSocket():
        def __init__(self):
            self.i = 0
        def __iter__(self):
            return self
        def __next__(self):
            if self.i > 2:
                raise OSError("You are done")
            self.i += 1
            return self.i

    # Ensure without the patch we actually get an error as we expect
    with pytest.raises(OSError):
        closing_iterator = werkzeug.wsgi.ClosingIterator(FakeSocket())
        for x in closing_iterator:
            assert x

    # Now with the patch we get no error. Yay.
    sepiida.wsgi.stabilize_werkzeug_iterator()
    closing_iterator = werkzeug.wsgi.ClosingIterator(FakeSocket())
    for x in closing_iterator:
        assert x
