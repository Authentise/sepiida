import json
import os.path
import tempfile
import urllib.parse
from uuid import uuid4

import pytest

import sepiida.storage


def test_upload_link_no_file(httpretty, settings):
    uuid = str(uuid4())
    key = uuid4()
    upload_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    httpretty.register_uri(
        httpretty.GET,
        urllib.parse.urljoin(settings.STORAGE_SERVICE, '/file/?filter[key]={}'.format(key)),
        body=json.dumps({'resources': []}),
        status=200,
    )
    httpretty.register_uri(
        httpretty.POST,
        'https://woodhouse.service/file/',
        body='',
        status=204,
        upload_location=upload_location,
    )

    assert sepiida.storage.upload_link(key, 'some-bucket') == upload_location

def test_upload_link_no_file_bad_response(httpretty, settings):
    key = uuid4()
    httpretty.register_uri(
        httpretty.GET,
        urllib.parse.urljoin(settings.STORAGE_SERVICE, '/file/?filter[key]={}'.format(key)),
        body=json.dumps({'resources': []}),
        status=200,
    )
    httpretty.register_uri(
        httpretty.POST,
        'https://woodhouse.service/file/',
        body='some error text',
        status=400,
    )

    with pytest.raises(sepiida.storage.FileStorageUnavailableException) as error:
        sepiida.storage.upload_link(key, 'some-bucket')

    assert error.value.errors[0].title == 'Unknown storage error: some error text'

def test_upload_link_file(httpretty, settings):
    uuid = str(uuid4())
    key = uuid4()
    upload_location = urllib.parse.urljoin(settings.STORAGE_SERVICE, '/content/{}/'.format(uuid))
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body=json.dumps({'resources': [{'upload-location': upload_location}]}),
        status=200,
    )

    assert sepiida.storage.upload_link(key, 'some-bucket') == upload_location

def test_upload_link_file_with_longer_than_2000_character_url(httpretty):
    uuid = str(uuid4())
    key = ','.join([str(uuid4()) for _ in range(100)])
    upload_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/',
        data='filter[key]={}'.format(key),
        body=json.dumps({'resources': [{'upload-location': upload_location}]}),
        status=200,
    )
    assert sepiida.storage.upload_link(key, 'some-bucket') == upload_location
    assert httpretty.last_request().body.decode() == 'filter[key]={}'.format(key)

def test_upload_link_bad_get_response(httpretty):
    key = uuid4()
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body='another error text',
        status=400,
    )

    with pytest.raises(sepiida.storage.StorageException) as error:
        sepiida.storage.upload_link(key, 'some-bucket')

    assert error.value.args[0] == 'Unknown storage error: another error text'

def test_upload_link_file_already_uploaded(httpretty):
    uuid = str(uuid4())
    key = uuid4()
    download_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body=json.dumps({'resources': [{'content': download_location, 'uploaded': 'lets pretend this is a datetime for truthyness'}]}),
        status=200,
    )

    with pytest.raises(sepiida.storage.StorageException) as error:
        sepiida.storage.upload_link(key, 'some-bucket')

    assert error.value.args[0] == 'A file with key {} has already been uploaded'.format(key)

def test_upload_link_file_race_condition_duplicate_key(httpretty):
    key = uuid4()
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body=json.dumps({'resources': []}),
        status=200,
    )
    httpretty.register_uri(
        httpretty.POST,
        'https://woodhouse.service/file/',
        body=json.dumps({"errors": [{
            "title": "some cryptic error body",
            "code": "DuplicateKeyError"
        }]}),
        status=400,
    )

    with pytest.raises(sepiida.storage.AlreadyUploadedException) as error:
        sepiida.storage.upload_link(key, 'some-bucket')

    assert error.value.args[0] == 'A file with key {} has already been uploaded'.format(key)

def test_upload_link_file_race_condition_no_json_errors(httpretty):
    key = uuid4()
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body=json.dumps({'resources': []}),
        status=200,
    )
    httpretty.register_uri(
        httpretty.POST,
        'https://woodhouse.service/file/',
        body='not a json error',
        status=500,
    )

    with pytest.raises(sepiida.storage.FileStorageUnavailableException) as error:
        sepiida.storage.upload_link(key, 'some-bucket')

    assert error.value.errors[0].title =='Upload fail due to network traffic issue, please try again'

def test_download_link(httpretty):
    uuid = str(uuid4())
    key = uuid4()
    download_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body=json.dumps({'resources': [{'content': download_location}]}),
        status=200,
    )

    assert sepiida.storage.download_link(key) == download_location

def test_download_link_file_with_longer_than_2000_character_url(httpretty):
    uuid = str(uuid4())
    key = ','.join([str(uuid4()) for _ in range(100)])
    download_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/',
        data='?filter[key]={}'.format(key),
        body=json.dumps({'resources': [{'content': download_location}]}),
        status=200,
    )

    assert sepiida.storage.download_link(key) == download_location
    assert httpretty.last_request().body.decode() == 'filter[key]={}'.format(key)

def test_download_link_not_uploaded(httpretty):
    uuid = str(uuid4())
    key = uuid4()
    upload_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body=json.dumps({'resources': [{'upload-location': upload_location}]}),
        status=200,
    )

    with pytest.raises(sepiida.storage.NotUploadedException) as error:
        sepiida.storage.download_link(key)

    assert error.value.args[0] == 'A file with key {} has not yet been uploaded'.format(key)

def test_download_link_no_file(httpretty):
    key = uuid4()
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body=json.dumps({'resources': []}),
        status=200,
    )

    with pytest.raises(sepiida.storage.NoFileException) as error:
        sepiida.storage.download_link(key)

    assert error.value.args[0] == 'A file with key {} does not exist in the database'.format(key)

def test_download_link_get_error(httpretty):
    key = uuid4()
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body='some error text',
        status=400,
    )

    with pytest.raises(sepiida.storage.FileStorageUnavailableException) as error:
        sepiida.storage.download_link(key)

    assert error.value.errors[0].title =='Unknown storage error: some error text'


def test_download_link_get_error_500(httpretty):
    key = uuid4()
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/?filter[key]={}'.format(key),
        body='some error text',
        status=500,
    )

    with pytest.raises(sepiida.storage.FileStorageUnavailableException) as error:
        sepiida.storage.download_link(key)

    assert error.value.errors[0].title =='Download fail due to network traffic issue, please try again'

def test_get_contents(httpretty):
    uuids = [str(uuid4()) for _ in range(100)]
    keys  = [str(uuid4()) for _ in range(100)]
    files_with_upload_links = [{
        'uri' : 'https://woodhouse.service/file/{}/'.format(uuids[i]),
        'upload-location': 'https://woodhouse.service/content/{}/'.format(uuids[i]),
        'key': keys[i],
    } for i in range(50)]
    files_with_content = [{
        'uri' : 'https://woodhouse.service/file/{}/'.format(uuids[i]),
        'content': 'https://woodhouse.service/content/{}/'.format(uuids[i]),
        'key': keys[i],
    } for i in range(51, 90)]
    files = files_with_upload_links + files_with_content
    httpretty.register_uri(
        httpretty.GET,
        'https://woodhouse.service/file/',
        data='?filter[key]={}'.format(keys),
        body=json.dumps({'resources': files}),
        status=200,
    )
    expected = {file['key'] : file for file in files}
    assert sepiida.storage.get_files(keys) == expected
    assert httpretty.last_request().body.decode() == 'filter[key]={}'.format(','.join(keys))

def test_put_success(httpretty, mocker):
    uuid = str(uuid4())
    key = uuid4()
    upload_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    mocker.patch('sepiida.storage.upload_link', return_value=upload_location)
    httpretty.register_uri(
        httpretty.PUT,
        upload_location,
        body='',
        status=204,
    )

    sepiida.storage.put(key, 'some-bucket', b'1234', 'some-mimetype')

    assert httpretty.last_request().body == b'1234'
    assert httpretty.last_request().path == '/content/{}/'.format(uuid)
    assert httpretty.last_request().headers['Host'] == 'woodhouse.service'
    assert httpretty.last_request().headers['Content-Type'] == 'some-mimetype'

def test_put_error(httpretty, mocker):
    uuid = str(uuid4())
    key = uuid4()
    upload_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    mocker.patch('sepiida.storage.upload_link', return_value=upload_location)
    httpretty.register_uri(
        httpretty.PUT,
        upload_location,
        body='some error text',
        status=400,
    )

    with pytest.raises(sepiida.storage.StorageException) as error:
        sepiida.storage.put(key, 'some-bucket', b'1234', 'some-mimetype')

    assert error.value.args[0] == 'Unknown storage error: some error text'

def test_get_to_stream(httpretty, mocker):
    uuid = str(uuid4())
    key = uuid4()
    download_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    mocker.patch('sepiida.storage.download_link', return_value=download_location)
    httpretty.register_uri(
        httpretty.GET,
        download_location,
        body=b'1234',
        status=200,
        content_type='some-mimetype',
    )

    with sepiida.storage.get(key) as response:
        assert response.read() == b'1234' # pylint: disable=no-member
    assert httpretty.last_request().path == '/content/{}/'.format(uuid)
    assert httpretty.last_request().headers['Host'] == 'woodhouse.service'

def test_get_s3_to_file(httpretty, mocker):
    uuid = str(uuid4())
    key = uuid4()
    download_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    mocker.patch('sepiida.storage.download_link', return_value=download_location)
    httpretty.register_uri(
        httpretty.GET,
        download_location,
        body=b'1234',
        status=200,
        content_type='some-mimetype',
    )

    with tempfile.TemporaryDirectory() as temp_path:
        filename = os.path.join(temp_path, str(key))
        sepiida.storage.get(key, filename)
        contents = open(filename, 'rb').read()
    assert contents == b'1234'

def test_get_error(httpretty, mocker):
    uuid = str(uuid4())
    key = uuid4()
    download_location = 'https://woodhouse.service/content/{}/'.format(uuid)
    mocker.patch('sepiida.storage.download_link', return_value=download_location)
    httpretty.register_uri(
        httpretty.GET,
        download_location,
        body='some error text',
        status=400,
        content_type='some-mimetype',
    )

    with pytest.raises(sepiida.storage.StorageException) as error:
        sepiida.storage.get(key)

    assert error.value.args[0] == 'Unknown storage error: some error text'
