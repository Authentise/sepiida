import sepiida.responses


def test_make_response(app, client):
    @app.route('/test-make-response/')
    def _make_response(): # pylint: disable=unused-variable
        return sepiida.responses.json_response({'test': 'a'})
    response = client.get('/test-make-response/')
    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert response.json == {'test': 'a'}
