import logging

import sepiida.log


def test_log_without_sentry():
    logging.getLogger().handlers = []
    assert not logging.getLogger().handlers
    sepiida.log.setup_logging()
    assert len(logging.getLogger().handlers) == 1
