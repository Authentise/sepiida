import json

import pytest

import sepiida.endpoints
from sepiida import errors

MyAPIError1 = errors.api_error()
MyAPIError2 = errors.api_error(title="Some message")
MyAPIError3 = errors.api_error(title="Another message", status_code=401)
MyAPIError4 = errors.api_error(title="Yet more message", status_code=402, error_code='error-code')

def test_error_basic():
    with pytest.raises(MyAPIError1) as e:
        raise MyAPIError1(title="Something happened")
    assert e.value.title == "Something happened"
    assert e.value.status_code == 400
    assert e.value.error_code == 'error'

def test_error_override_message():
    with pytest.raises(MyAPIError2) as e:
        raise MyAPIError2()
    assert e.value.title == "Some message"
    assert e.value.status_code == 400
    assert e.value.error_code == 'error'

    with pytest.raises(MyAPIError2) as e:
        raise MyAPIError2(title="Something else happened")
    assert e.value.title == "Something else happened"
    assert e.value.status_code == 400
    assert e.value.error_code == 'error'

    with pytest.raises(MyAPIError2) as e:
        raise MyAPIError2()
    assert e.value.title == "Some message"
    assert e.value.status_code == 400
    assert e.value.error_code == 'error'

def test_error_override_status():
    with pytest.raises(MyAPIError3) as e:
        raise MyAPIError3()
    assert e.value.title == "Another message"
    assert e.value.status_code == 401
    assert e.value.error_code == 'error'

    with pytest.raises(MyAPIError3) as e:
        raise MyAPIError3(status_code=400)
    assert e.value.title == "Another message"
    assert e.value.status_code == 400
    assert e.value.error_code == 'error'

def test_error_override_code():
    with pytest.raises(MyAPIError4) as e:
        raise MyAPIError4()
    assert e.value.title == "Yet more message"
    assert e.value.status_code == 402
    assert e.value.error_code == 'error-code'

    with pytest.raises(MyAPIError4) as e:
        raise MyAPIError4(error_code='different-code')
    assert e.value.title == "Yet more message"
    assert e.value.status_code == 402
    assert e.value.error_code == 'different-code'

def test_error_with_flask_restful(app, json_client):
    app.config['TESTING'] = False
    app.config['DEBUG'] = False
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            raise MyAPIError1()
    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 400
    text = response.data.decode('utf-8')
    data = json.loads(text)
    assert data == {
        'errors'    : [{
            'code'  : 'error',
            'title' : 'An error occurred',
        }]
    }

def test_error_data_property():
    my_error = errors.api_error(title='something', status_code=404, error_code='a-test')
    api_error = errors.APIException(errors=[my_error()])
    assert api_error.data == {
        'errors': [{
            'title' : 'something',
            'code'  : 'a-test',
        }]
    }
    assert api_error.code == 404

def test_rate_limit_error(app, json_client):
    app.config['TESTING'] = False
    app.config['DEBUG'] = False
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        @staticmethod
        def list():
            raise errors.RateLimited(3.1)
    TestEndpoint.add_resource(app)
    response = json_client.get('/test-endpoint/')
    assert response.status_code == 429
    assert response.headers['Cooldown'] == '3.1'
    text = response.data.decode('utf-8')
    data = json.loads(text)
    assert data == {
        'errors'    : [{
            'code'  : 'rate-limited',
            'title' : 'You are attempting to request this API too quickly. You need to wait 3.1 more seconds before retrying',
        }]
    }
