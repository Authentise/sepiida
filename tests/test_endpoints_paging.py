import urllib.parse

import sepiida.endpoints
import sepiida.fields

def _setup_test_endpoint(app, max_=12):
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        DEFAULT_LIMIT  = max_
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Integer()
        def list(self):
            return list(range(self.offset, min(100, self.offset + self.limit)))
    TestEndpoint.add_resource(app)

def _assert_link(link, query):
    _, link_query = urllib.parse.splitquery(link)
    parts = urllib.parse.parse_qs(link_query)
    for k, v in query.items():
        assert parts[k] == [str(v)]

def test_default_limit(app, client):
    "Ensure that by default list endpoints apply a limit of 20"
    class TestEndpoint(sepiida.endpoints.APIEndpoint):
        ENDPOINT = '/test-endpoint/'
        SIGNATURE = sepiida.fields.Integer()
        def list(self):
            return list(range(self.offset, min(self.limit, 100)))
    TestEndpoint.add_resource(app)
    response = client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json['resources'] == list(range(20))

def test_default_limit_override(app, client):
    "Ensure that we can override the default limit"
    _setup_test_endpoint(app, max_=12)

    response = client.get('/test-endpoint/')
    assert response.status_code == 200
    assert response.json['resources'] == list(range(12))
    _assert_link(response.json['links']['self'], {
        'page[offset]' : 0,
        'page[limit]'  : 12,
    })
    _assert_link(response.json['links']['next'], {
        'page[offset]' : 12,
        'page[limit]'  : 12,
    })
    assert response.json['links']['prev'] is None
