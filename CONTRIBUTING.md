Overview
====
Sepiida is not so useful on it's own. Most use/testing/changes should be done in the context of services using it, 
for example, nautilus. 

How to Build
====
Do not build/run sepiida on it' own.  Please use it as part of another service.   There is a 
test docker builder below, just for testing.

How to Test (fish shell version)
====

1. Build  a test docker image: 
  * docker build -t sepiida_env_test .

2. Run the test image (over-mounting src with the local db)
  * docker run -v (pwd):/src -it sepiida_env_test:latest /usr/bin/fish 

3. clear 'host' compiled data, to avoid test errors 
  * find . -name \*.pyc -delete

4. run pytests tests in the image
 * pytest  tests/

5. Profit! 

How to Test (virtualenv version) 
=====

Make a virtualenv:  
  virtualenv --python=python3 venv_3  
Start the virtualenv 
  . ./venv_3/bin/activate.fish    

in the venv  pytests in the test directory: 
  pytest tests 

How to build a release
====
 1. Tag the release with a version nummber

 2. run the virtualenv as per 'How to Test virtualenv version'

 3. run 'build.fish'

 4.  follow 'twine' instructions from output upload to pypi
