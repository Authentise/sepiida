#!/usr/bin/env fish
#
echo "Removing dist/*"
rm -f dist/*

echo "Removing __pycache__ directories"
find . | grep -E "(__pycache__)" | xargs rm -rf

echo "Running pylint"
./pylint.sh

echo "Running unit tests"
py.test --cov-report xml --cov sepiida --junitxml dist/results.xml tests

echo "Building the python package"
set build_version (python setup.py version)

echo "Found build version '$build_version'"

if test $build_version = "0.0.1"
	echo "Building package without upload"
	python setup.py sdist
else
	echo "Building package and uploading"
	python setup.py sdist #upload
	#python setup.py sdist upload
	echo "try running  'twine upload --repository-url https://upload.pypi.org/legacy/ dist/sepiida-18.9.tar.gz  --verbose'"
	echo "pypi has changed, use twine to mnaually upload #may take some testing to get this to work"
end
echo "Making everything readable and writable"
chmod a+rw -R *
echo "done"
