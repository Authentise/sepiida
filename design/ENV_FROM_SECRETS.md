Secrets Design: 

Docker can create and store secrets vis their own secrets system. These add/create
secrets at the command line, and allowed processes access them at the location  
/run/secrets/<secret_name> 

We want to use that existing system to manages secrets for our servirs. 

1) Set a secret using the docker
  `printf "This is a secret" | docker secret create ENV_VARAIBLES_NAME -`

2) On load sepiida/environment.py will look for items in their  environment 
signature. If there is a matching entry in  SPECIFICATION, it will load data 
from the secret into the specification 


Example; 
Developer wants to add secret "WAWA_HOAGIE_ORDER_API_KEY" to the systems
1) Developer adds that secret to a environment.py (sepiida/environment.py or 
nautilus/environment.py) SIGNATURE with a default value 

2) Developer can set the value WAWA_HOAGIE_ORDER_API_KEY as a Local
environment during development. 

3) at deployment time, that value can be left as an environment value. OR, the 
developer or ops person can use docker to set a secret via 
`$ printf "WaWa Secret To Buy Hogies" | docker secret create WAWA_HOAGIE_ORDER_API_KEY -`

4) again, at the deployment time, new secrets must be forwarded from docker secrets store
to containers. This can be done via
`$ docker service update --secret-add source=WAWA_HOAGIE_ORDER_API_KEY,target=WAWA_HOAGIE_ORDER_API_KEY nautilus`
After that, `WAWA_HOAGIE_ORDER_API_KEY` file will appear at the `/run/secrets` directory of all nautilus service replicas.

5) At the next environment reload, the environment.py system will look to
match SIGNATURE (including the new  /run/secrets/WAWA_HOAGIE_ORDER_API_KEY). 
if it's found, it will read that file into SIGNATURE and convert it based on 
Signature Variable type.
